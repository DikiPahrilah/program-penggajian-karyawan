-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 01:41 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpenggajian`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_karyawan`
--

CREATE TABLE `data_karyawan` (
  `nomor` int(11) NOT NULL,
  `nip` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `golongan` varchar(10) NOT NULL,
  `jab_struktur` varchar(15) NOT NULL,
  `jab_fungsional` varchar(20) NOT NULL,
  `pend_akhir` varchar(10) NOT NULL,
  `status_kawin` varchar(10) NOT NULL,
  `jumlah_anak` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_karyawan`
--

INSERT INTO `data_karyawan` (`nomor`, `nip`, `nama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `golongan`, `jab_struktur`, `jab_fungsional`, `pend_akhir`, `status_kawin`, `jumlah_anak`) VALUES
(1, 1567800001, 'Alex', 'Medan', '1987-09-15', 'Bandung', '4A', 'SPM', 'Pengawas Barang', 'SMA', 'Belum Kawi', '0'),
(2, 1567800002, 'Ani Heriani', 'Bandung', '1988-06-06', 'Bandung Barat', '4A', 'SPG', 'Pemeriksa', 'SMA', 'Kawin', '2'),
(3, 1567800003, 'Andri Hermawan', 'Bandung', '1985-06-10', 'Kiaracondong', '3A', 'Salesman', 'Agen', 'D3', 'Kawin', '2'),
(4, 1567800004, 'Asrullah Zulpan', 'Lampung', '1992-07-15', 'Buah Batu', '1A', 'Supervisor (Mod', 'Analisa', 'S1', 'Belum Kawi', '0'),
(5, 1567800005, 'Artur', 'Bandung', '1987-09-15', 'Buah Batu', '4A', 'SPM', 'Pengawas Barang', 'SMA', 'Belum Kawi', '0'),
(6, 1567800006, 'Agus', 'Garut', '1975-08-20', 'Kiaracondong', '4A', 'SPM', 'Pemeriksa', 'SMA', 'Kawin', '2'),
(7, 1567800007, 'Edi', 'Bogor', '1984-05-08', 'Dayeuh Kolot', '4A', 'SPM', 'Pengawas Barang', 'SMA', 'Kawin', '2'),
(8, 1567800008, 'Elvin', 'Riau', '1988-05-17', 'Cibiru', '4A', 'SPM', 'Peneliti', 'SMK', 'Belum Kawi', '0'),
(9, 1567800009, 'Ervan', 'Bandung', '1987-09-10', 'Cibiru', '4A', 'SPM', 'Pengawas Barang', 'SMA', 'Belum Kawi', '0'),
(10, 1567800010, 'Firdi', 'Bandung', '1987-11-24', 'Darwati', '4A', 'SPM', 'Pemeriksa', 'SMK', 'Belum Kawi', '0'),
(11, 1567800011, 'Ferry', 'Bandung', '1985-05-15', 'Cibiru', '4A', 'SPM', 'Pemeriksa', 'D3', 'Belum Kawi', '0'),
(12, 1567800012, 'Ganjar', 'Bogor', '1988-12-28', 'Buah Batu', '4A', 'SPM', 'Peneliti', 'SMK', 'Kawin', '1'),
(13, 1567800013, 'Herna', 'Garut', '1988-06-13', 'Kiaracondong', '4A', 'SPG', 'Peneliti', 'SMA', 'Kawin', '1'),
(14, 1567800014, 'Irwan', 'Garut', '1986-06-18', 'Darwati', '4A', 'SPM', 'Pemeriksa', 'SMA', 'Kawin', '2'),
(15, 1567800015, 'Martunias', 'Medan', '1988-06-28', 'Batununggal', '4A', 'SPM', 'Pengawas Barang', 'SMK', 'Belum Kawi', '0'),
(16, 1567800016, 'Noverman', 'Bandung', '1987-12-21', 'Batununggal', '4A', 'SPM', 'Peneliti', 'SMA', 'Kawin', '2'),
(17, 1567800017, 'Nurzaman', 'Garut', '1989-09-19', 'Dayeuh Kolot', '4A', 'SPM', 'Pemeriksa', 'SMK', 'Belum Kawi', '0'),
(18, 1567800018, 'Nugie', 'Jakarta', '1987-06-16', 'Dayeuh Kolot', '4A', 'SPM', 'Peneliti', 'SMK', 'Kawin', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hitung_gaji`
--

CREATE TABLE `hitung_gaji` (
  `tgl_bayar` varchar(10) NOT NULL,
  `gaji_bln` varchar(15) NOT NULL,
  `nomor_slip` varchar(11) NOT NULL,
  `nip` int(11) NOT NULL,
  `nama_peg` varchar(30) NOT NULL,
  `tgl_lahir` varchar(10) NOT NULL,
  `gol` varchar(15) NOT NULL,
  `status_kawin` varchar(15) NOT NULL,
  `jumlah_anak` varchar(10) NOT NULL,
  `gapok` double NOT NULL,
  `tunjab_struk` double NOT NULL,
  `tunjab_fung` double NOT NULL,
  `tunj_keluarga` double NOT NULL,
  `tunj_anak` double NOT NULL,
  `total_ab` double NOT NULL,
  `pinj_koperasi` double NOT NULL,
  `iuran_peg` double NOT NULL,
  `pph` double NOT NULL,
  `gaji_bersih` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hitung_gaji`
--

INSERT INTO `hitung_gaji` (`tgl_bayar`, `gaji_bln`, `nomor_slip`, `nip`, `nama_peg`, `tgl_lahir`, `gol`, `status_kawin`, `jumlah_anak`, `gapok`, `tunjab_struk`, `tunjab_fung`, `tunj_keluarga`, `tunj_anak`, `total_ab`, `pinj_koperasi`, `iuran_peg`, `pph`, `gaji_bersih`) VALUES
('17/9/2018', 'September', '001', 1567800001, 'Alex', '1987-09-15', '4A', 'Belum Kawin', '0', 3000000, 300000, 300000, 0, 0, 3600000, 0, 0, 360000, 3240000),
('17/9/2018', 'September', '002', 1567800002, 'Ani Heriani', '1988-06-06', '4A', 'Kawin', '2', 3000000, 300000, 250000, 500000, 2000000, 6100000, 0, 0, 610000, 5490000),
('17/9/2018', 'September', '003', 1567800003, 'Andri Hermawan', '1985-06-10', '3A', 'Kawin', '2', 3000000, 400000, 500000, 500000, 2000000, 6300000, 0, 0, 630000, 5670000),
('17/9/2018', 'September', '004', 1567800004, 'Asrullah Zulpan', '1992-07-15', '1A', 'Belum Kawin', '0', 6000000, 600000, 500000, 0, 0, 7200000, 0, 0, 720000, 6480000),
('17/9/2018', 'September', '005', 1567800005, 'Artur', '1987-09-15', '4A', 'Belum Kawin', '0', 3000000, 300000, 300000, 0, 0, 3600000, 0, 0, 360000, 3240000),
('17/9/2018', 'September', '006', 1567800006, 'Agus', '1975-08-20', '4A', 'Kawin', '2', 3000000, 300000, 250000, 500000, 2000000, 6100000, 0, 0, 610000, 5490000),
('17/9/2018', 'September', '007', 1567800007, 'Edi', '1984-05-08', '4A', 'Kawin', '2', 3000000, 300000, 300000, 500000, 2000000, 6100000, 0, 0, 610000, 5490000),
('17/9/2018', 'September', '008', 1567800008, 'Elvin', '1988-05-17', '4A', 'Belum Kawin', '0', 3000000, 300000, 400000, 0, 0, 3600000, 0, 0, 360000, 3240000),
('17/9/2018', 'September', '009', 1567800009, 'Ervan', '1987-09-10', '4A', 'Belum Kawin', '0', 3000000, 300000, 300000, 0, 0, 3600000, 0, 0, 360000, 3240000),
('17/9/2018', 'September', '0010', 1567800010, 'Firdi', '1987-11-24', '4A', 'Belum Kawin', '0', 3000000, 300000, 250000, 0, 0, 3600000, 0, 0, 360000, 3240000);

-- --------------------------------------------------------

--
-- Table structure for table `jab_fungsional`
--

CREATE TABLE `jab_fungsional` (
  `kd_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(15) NOT NULL,
  `tunj_jabatan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jab_fungsional`
--

INSERT INTO `jab_fungsional` (`kd_jabatan`, `nama_jabatan`, `tunj_jabatan`) VALUES
(1, 'Analisa', 500000),
(2, 'Agen', 500000),
(3, 'Peneliti', 400000),
(4, 'Pemeriksa', 250000),
(5, 'Pengawas Barang', 300000);

-- --------------------------------------------------------

--
-- Table structure for table `jab_struktural`
--

CREATE TABLE `jab_struktural` (
  `kd_jab` int(11) NOT NULL,
  `nama_jab` varchar(15) NOT NULL,
  `tunj_jab` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jab_struktural`
--

INSERT INTO `jab_struktural` (`kd_jab`, `nama_jab`, `tunj_jab`) VALUES
(1, 'Admin (Infoice)', 500000),
(2, 'Admin (Sales Su', 500000),
(3, 'Admin (Service)', 400000),
(4, 'Bagian Umum', 250000),
(5, 'Manajer', 700000),
(6, 'Salesman', 400000),
(7, 'SPM', 300000),
(8, 'SPG', 300000),
(9, 'Supervisor (Mod', 600000),
(10, 'Tekhnisi', 300000);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `nama_admin` varchar(11) NOT NULL,
  `username` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`nama_admin`, `username`, `password`) VALUES
('Administrat', 'Admin', 'f6fdffe48c9');

-- --------------------------------------------------------

--
-- Table structure for table `pangkat_gol`
--

CREATE TABLE `pangkat_gol` (
  `kd_pangkat` int(11) NOT NULL,
  `nama_pangkat` varchar(15) NOT NULL,
  `golongan` varchar(10) NOT NULL,
  `jml_gapok` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pangkat_gol`
--

INSERT INTO `pangkat_gol` (`kd_pangkat`, `nama_pangkat`, `golongan`, `jml_gapok`) VALUES
(1, 'Admin (Infoice)', '1A', 5000000),
(2, 'Admin (Sales Su', '1A', 5000000),
(3, 'Admin (Service)', '2A', 4000000),
(4, 'Bagian Umum', '2A', 2500000),
(5, 'Manajer', '1A', 7000000),
(6, 'Salesman', '3A', 4000000),
(7, 'SPM', '4A', 3000000),
(8, 'SPG', '4A', 3000000),
(9, 'Supervisor (Mod', '1A', 6000000),
(10, 'Tekhnisi', '3A', 3000000);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `kd_pend` int(11) NOT NULL,
  `jenjang_pend` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`kd_pend`, `jenjang_pend`) VALUES
(1, 'S1'),
(2, 'D3'),
(3, 'SMA'),
(4, 'SMK'),
(5, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `status_kawin`
--

CREATE TABLE `status_kawin` (
  `kd_status` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `tunj_kawin` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_kawin`
--

INSERT INTO `status_kawin` (`kd_status`, `status`, `tunj_kawin`) VALUES
(1, 'Kawin', 500000),
(2, 'Belum Kawin', 0),
(3, 'Duda', 250000),
(4, 'Randa', 250000);

-- --------------------------------------------------------

--
-- Table structure for table `tunj_anak`
--

CREATE TABLE `tunj_anak` (
  `id` int(11) NOT NULL,
  `jumlah_anak` varchar(11) NOT NULL,
  `jml_tunjangan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tunj_anak`
--

INSERT INTO `tunj_anak` (`id`, `jumlah_anak`, `jml_tunjangan`) VALUES
(1, '0', 0),
(2, '1', 1000000),
(3, '2', 2000000),
(4, '3', 3000000),
(5, '4', 4000000),
(6, '5', 5000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  ADD PRIMARY KEY (`nomor`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `hitung_gaji`
--
ALTER TABLE `hitung_gaji`
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `jab_fungsional`
--
ALTER TABLE `jab_fungsional`
  ADD PRIMARY KEY (`kd_jabatan`);

--
-- Indexes for table `jab_struktural`
--
ALTER TABLE `jab_struktural`
  ADD PRIMARY KEY (`kd_jab`);

--
-- Indexes for table `pangkat_gol`
--
ALTER TABLE `pangkat_gol`
  ADD PRIMARY KEY (`kd_pangkat`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`kd_pend`);

--
-- Indexes for table `status_kawin`
--
ALTER TABLE `status_kawin`
  ADD PRIMARY KEY (`kd_status`);

--
-- Indexes for table `tunj_anak`
--
ALTER TABLE `tunj_anak`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `jab_fungsional`
--
ALTER TABLE `jab_fungsional`
  MODIFY `kd_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jab_struktural`
--
ALTER TABLE `jab_struktural`
  MODIFY `kd_jab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pangkat_gol`
--
ALTER TABLE `pangkat_gol`
  MODIFY `kd_pangkat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `kd_pend` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status_kawin`
--
ALTER TABLE `status_kawin`
  MODIFY `kd_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tunj_anak`
--
ALTER TABLE `tunj_anak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hitung_gaji`
--
ALTER TABLE `hitung_gaji`
  ADD CONSTRAINT `hitung_gaji_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `data_karyawan` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
