# PROGRAM PENGGAJIAN KARYAWAN BERBASIS JAVA DESKTOP


![Alt text](/src/Images/menu-utama.PNG?raw=true "Penggajian Karyawan")

## ScreenShoot Program Penggajian Karyawan.

- Tampilan Login

![Alt text](/src/Images/login-program.PNG?raw=true "Tampilan Login")

- Tampilan Menu Utama Program

![Alt text](/src/Images/menu-utama.PNG?raw=true "Tampilan Menu Utama Program")

- Tampilan Master Jabatan Fungsional.

![Alt text](/src/Images/form-master-jabatan-fungsi.PNG?raw=true "Tampilan Master Jabatan Fungsional")

- Tampilan Master Jabatan Struktural.
   
![Alt text](/src/Images/form-master-jabatan-struk.PNG?raw=true "Tampilan Master Jabtan Struktural")

- Tampilan Status Perkawinan.

![Alt text](/src/Images/form-master-status-perkawinan.PNG?raw=true "Tampilan Form Status Perkawinan")

- Tampilan Status Pendidikan

![Alt text](/src/Images/form-master-pendidikan.PNG?raw=true "Tampilan Form Status Pendidikan")

- Tampilan Data Golongan

![Alt text](/src/Images/form-master-data-gol.PNG?raw=true "Tampilan Form Data Golongan")

- Tampilan Input Data Karyawan

![Alt text](/src/Images/form-input-data-karyawan.PNG?raw=true "Tampilan Form Input Data Karyawan")

- Tampilan Update Data Karyawan

![Alt text](/src/Images/form-update-data-karyawan.PNG?raw=true "Tampilan Form Update Data Karyawan")

- Tampilan Hitung Gaji Karyawan

![Alt text](/src/Images/form-hitung-gaji.PNG?raw=true "Tampilan Form Hitung Gaji Karyawan")

- Tampilan Master Data Karyawan

![Alt text](/src/Images/form-master-data-karyawan.PNG?raw=true "Tampilan Form Master Data Karyawan")

- Tampilan Master Tabel Gaji

![Alt text](/src/Images/form-master-tabel-gaji.PNG?raw=true "Tampilan Form Master Tabel Gaji")

- Tampilan Cetak Perbulan / Keseluruhan

![Alt text](/src/Images/form-cetak-perbulan.PNG?raw=true "Tampilan Form Cetak Perbulan / Keseluruhan")

- Tampilan Laporan Slip Karyawan

![Alt text](/src/Images/laporan-slip-karyawan.PNG?raw=true "Tampilan Laporan Slip Karyawan")


### Untuk Tampilan Bisa disesuiakan.

- Gambaran atau Schema isi dari Program Penggajian Karyawan.

```shell
PENGGAJIAN KARYAWAN                 			# → Root Directory
└── src/
    ├── Data/
    │   ├── CetakLaporan.form
    │   └── CetakLaporan.java
    │   ├── CetakSlip.form
    │   └── CetakSlip.java
	│	├── DataKaryawan.form
    │   └── DataKaryawan.java
	│	├── DataKehadiran.form
    │   └── DataKehadiran.java
	│	├── HitungGaji.form
    │   └── HitungGaji.java
	│	├── ShowUbahDataKaryawan.form
    │   └── ShowUbahDataKaryawan.java
	│	├── UbahDataKaryawan.form
    │   └── UbahDataKaryawan.java
	│	├── UbahDataKehadiran.form
    │   └── UbahDataKehadiran.java
    ├── Encrypt/
    │   ├── encrypt_password.form
    │   └── encrypt_password.java
	├── EnxampleOpenTabel/
    │   ├── Java_jtable_data.form
    │   └── Java_jtable_data.java
	│	├── RunningText.form
    │   └── RunningText.java
	│	├── Show_Jtable_row_data.form
    │   └── Show_Jtable_row_data.java
	├── GmailClient/
    │   ├── EmailMainGUI.form
    │   └── EmailMainGUI.java
	│ 	└── SendEmail.java
	├── Icon/
	├── IconMenuBar/
	├── Images/
	├── Koneksi/
	│	└── koneksi.java
	├── Laporan/
	├── LaporanGaji/
	├── Login/
	│	├── CaptchaGenerator.java
    │   └── EncryptPassword.form
	│	├── EncryptPassword.java
    │   └── LoginCaptchaDB.form
	│	├── LoginCaptchaDB.java
    │   └── LoginHash.java
	├── Master/
	│	├── MasterDataGolongan.form
    │   └── MasterDataGolongan.java
	│	├── MasterDataJabatan.form
    │   └── MasterDataJabatan.java
	│	├── MasterDataJabatanFungsional.form
    │   └── MasterDataJabatanFungsional.java
	│	├── MasterDataKaryawan.form
    │   └── MasterDataKaryawan.java
	│	├── MasterDataPendidikan.form
    │   └── MasterDataPendidikan.java
	│	├── MasterDataStatus.form
    │   └── MasterDataStatus.java
	├── MenuUtama/
	│ 	├── Menu_Utama.form
    │   └── Menu_Utama.java
	│	├── TambahAdmin.form
    │   └── TambahAdmin.java
    ├── ScanData
	│	├── DbConn.java
    │   └── Main.form
    │  	└── Main.java
    └── SwingFileDownloadHTTP/					# → Various src files
		└── download/ 
		│  	├── DownloadTask.java
		│   └── HTTPDownloadUtil.java
      	│   └── SwingFileDownloadHTTP.java
		├── FileTypeFilter.Java
		└── JFilePicker.java
```		

## For Help
Jika ada masalah atau kurang jelas saat penggunaan aplikasi bisa langsung hubungi saya. :)

	Nama          =>        Diki Pahrilah
	Kelas         =>        4 IF - 01
	NRP	          =>        150613019

Thank's to :

Saya Sendiri, Teman - teman Seperjuangan.

### Created by : 

Created and Edited by :
Diki Pahrilah.
2018