/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Master;

import Data.DataKaryawan;
import Data.ShowUbahDataKaryawan;
import Data.UbahDataKaryawan;
import Koneksi.koneksi;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FachrealHeart
 */
public class MasterDataKaryawan extends javax.swing.JFrame {
JasperReport jasperReport; 
JasperDesign jasperDesign;
JasperPrint jasperPrint;

Map <String,Object> parham = new HashMap<String,Object>();
Connection conn = new koneksi().getKoneksi();
 ResultSet r;
    Statement s;
    
    private Object [][] datakaryawan=null;
    private String[] label={"No.","NIP","Nama Karyawan","Tempat Lahir","Tanggal Lahir","Alamat","Pangkat/Gol.","Jab. Struktural","Jab. Fungsional","Pend. Terakhir","Status","Jml_anak"};
   
    /**
     * Creates new form MasterDataPegawai
     */
    public MasterDataKaryawan() {
        initComponents();
        txt_cari_karyawan.setToolTipText("Masukkan <NIP> Karyawan disini");
        btn_cari.setToolTipText("Cari Data");
        btn_tambah.setToolTipText("Tambah Data");
        btn_update.setToolTipText("Update Data");
        btn_cetak.setToolTipText("Cetak Data");
        btn_keluar.setToolTipText("Keluar");
        bacatabelkaryawan();
    }
    
    private  void bacatabelkaryawan(){
       try {
           s=conn.createStatement();
           String sql="select * from data_karyawan";
           r=s.executeQuery(sql);
           ResultSetMetaData m= r.getMetaData();
           int kolom=m.getColumnCount();
           int baris=0;
           while(r.next()){
               baris=r.getRow();
           }
           datakaryawan=new Object[baris][kolom];
           int x=0;
           r.beforeFirst();
           while(r.next()){
               datakaryawan[x][0]=r.getString("nomor");
               datakaryawan[x][1]=r.getString("nip");
               datakaryawan[x][2]=r.getString("nama");
               datakaryawan[x][3]=r.getString("tempat_lahir");
               datakaryawan[x][4]=r.getString("tgl_lahir");
               datakaryawan[x][5]=r.getString("alamat");
               datakaryawan[x][6]=r.getString("golongan");
               datakaryawan[x][7]=r.getString("jab_struktur");
               datakaryawan[x][8]=r.getString("jab_fungsional");
               datakaryawan[x][9]=r.getString("pend_akhir");
               datakaryawan[x][10]=r.getString("status_kawin");
               datakaryawan[x][11]=r.getString("jumlah_anak");
               x++;
           }
           jTable2.setModel(new DefaultTableModel(datakaryawan,label));
       }catch(SQLException e){
           JOptionPane.showMessageDialog(null,e);
       }
   }
     
       private  void cari_karyawan(){
       try {
           s=conn.createStatement();
           String sql="select * from data_karyawan where nip='"+txt_cari_karyawan.getText()+"'";
           r=s.executeQuery(sql);
           ResultSetMetaData m= r.getMetaData();
           int kolom=m.getColumnCount();
           int baris=0;
           while(r.next()){
               baris=r.getRow();
           }
           datakaryawan=new Object[baris][kolom];
           int x=0;
           r.beforeFirst();
           while(r.next()){
               datakaryawan[x][0]=r.getString("nomor");
               datakaryawan[x][1]=r.getString("nip");
               datakaryawan[x][2]=r.getString("nama");
               datakaryawan[x][3]=r.getString("tempat_lahir");
               datakaryawan[x][4]=r.getString("tgl_lahir");
               datakaryawan[x][5]=r.getString("alamat");
               datakaryawan[x][6]=r.getString("golongan");
               datakaryawan[x][7]=r.getString("jab_struktur");
               datakaryawan[x][8]=r.getString("jab_fungsional");
               datakaryawan[x][9]=r.getString("pend_akhir");
               datakaryawan[x][10]=r.getString("status_kawin");
               datakaryawan[x][11]=r.getString("jumlah_anak");
               x++;
           }
           jTable2.setModel(new DefaultTableModel(datakaryawan,label));
       }catch(SQLException e){
           JOptionPane.showMessageDialog(null,e);
       }
   }
       
    
       private void settabel(){
       int kolom=jTable2.getSelectedRow();
       txt_cari_karyawan.setText((String)jTable2.getValueAt(kolom,1));
           }

       
    // untuk menampilkan Form ShowUbahDataKaryawan ketika tabel nama di klik.
    ShowUbahDataKaryawan jDataKaryawan = new ShowUbahDataKaryawan();

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_cari_karyawan = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        btn_tambah = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        btn_cetak = new javax.swing.JButton();
        btn_keluar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("MASTER DATA KARYAWAN");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(235, 30, 468, 54));

        jPanel2.setBackground(new java.awt.Color(0, 204, 204));
        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setForeground(new java.awt.Color(204, 255, 51));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("Cari Karyawan Berdasarkan NIP  :");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 190, 20));
        jPanel2.add(txt_cari_karyawan, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 210, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 100, 430, 50));

        btn_cari.setText("Cari");
        btn_cari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });
        jPanel1.add(btn_cari, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 100, 100, 40));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 760, 290));

        btn_tambah.setText("TAMBAH");
        btn_tambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahActionPerformed(evt);
            }
        });
        jPanel1.add(btn_tambah, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 470, 160, 40));

        btn_update.setText("UBAH DATA");
        btn_update.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        jPanel1.add(btn_update, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 470, 140, 40));

        btn_cetak.setText("CETAK DATA");
        btn_cetak.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cetakActionPerformed(evt);
            }
        });
        jPanel1.add(btn_cetak, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 470, 190, 40));

        btn_keluar.setText("KELUAR");
        btn_keluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });
        jPanel1.add(btn_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 470, 120, 40));

        jLabel3.setText("Poto");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 210, 110));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 783, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 577, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void btn_tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahActionPerformed
        // TODO add your handling code here:
        new DataKaryawan().show();
        this.dispose();
    }//GEN-LAST:event_btn_tambahActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        // TODO add your handling code here:
        new UbahDataKaryawan().show();
        this.dispose();
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cetakActionPerformed
        // TODO add your handling code here:
        try { 
         
            File file = new File("src/Laporan/LaporanKaryawan.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            parham.clear();
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport,parham,conn);
            JasperViewer.viewReport(jasperPrint, false);
            } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Data tidak dapat dicetak!"+ "\n" + e.getMessage(), "Cetak Data", JOptionPane.ERROR_MESSAGE);
        }          
        
    }//GEN-LAST:event_btn_cetakActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        int index = jTable2.getSelectedRow();
        TableModel model = jTable2.getModel();
        
        String no = model.getValueAt(index, 0).toString();
        String nip = model.getValueAt(index, 1).toString();
        String nama = model.getValueAt(index, 2).toString();
        String tempat = model.getValueAt(index, 3).toString();
        String tanggal = model.getValueAt(index, 4).toString();
        String alamat = model.getValueAt(index, 5).toString();
        String pangkat_gol = model.getValueAt(index, 6).toString();
        String jabatan_struk = model.getValueAt(index, 7).toString();
        String jabatan_fungsi = model.getValueAt(index, 8).toString();
        String pendidikan = model.getValueAt(index, 9).toString();
        String status = model.getValueAt(index, 10).toString();
        String jumlah_anak = model.getValueAt(index, 11).toString();
      
        
        jDataKaryawan.setVisible(true);
        jDataKaryawan.pack();
        jDataKaryawan.setLocationRelativeTo(null);
        jDataKaryawan.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        jDataKaryawan.txt_no_urut.setText(no);
        jDataKaryawan.txt_nip.setText(nip);
        jDataKaryawan.txt_nama_karyawan.setText(nama);
        jDataKaryawan.txt_ttl.setText(tempat);
        java.util.Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(tanggal);
        } catch (ParseException ex) {
            Logger.getLogger(MasterDataKaryawan.class.getName()).log(Level.SEVERE, null, ex);
        }
        jDataKaryawan.jDateChooser1.setDate(date); 
        jDataKaryawan.txt_alamat.setText(alamat);
        jDataKaryawan.comboPangkatGol.setSelectedItem(pangkat_gol);
        jDataKaryawan.comboJabatanStruk.setSelectedItem(jabatan_struk);
        jDataKaryawan.comboJabatanFungsi.setSelectedItem(jabatan_fungsi);
        jDataKaryawan.comboPendidikan.setSelectedItem(pendidikan);
        jDataKaryawan.comboStatus.setSelectedItem(status);
        jDataKaryawan.comboJumlahAnak.setSelectedItem(jumlah_anak);
          
    }//GEN-LAST:event_jTable2MouseClicked

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        cari_karyawan();
    }//GEN-LAST:event_btn_cariActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MasterDataKaryawan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_cetak;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_tambah;
    private javax.swing.JButton btn_update;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField txt_cari_karyawan;
    // End of variables declaration//GEN-END:variables
}
