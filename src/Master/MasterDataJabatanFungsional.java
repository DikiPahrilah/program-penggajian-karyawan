/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Master;

import Koneksi.koneksi;
import com.mysql.jdbc.PreparedStatement;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FachrealHeart
 */
public class MasterDataJabatanFungsional extends javax.swing.JFrame {
private DefaultTableModel model;
    /**
     * Creates new form MasterDataJabatanFungsional
     */
JasperReport jasperReport; 
JasperDesign jasperDesign;
JasperPrint jasperPrint;

Map <String,Object> parham = new HashMap<String,Object>();
Connection conn = new koneksi().getKoneksi();

    public MasterDataJabatanFungsional() {
        initComponents();
        
        autonumber();
        txt_nama_jabatan.setToolTipText("Silahkan Masukkan nama jabatan fungsional disini");
        txt_jumlah_tunjangan.setToolTipText("Silahkan Masukkan jumlah tunjangan karyawan");
        btnTambah.setToolTipText("Tambah Data");
        btnUbah.setToolTipText("Ubah Data");
        btnHapus.setToolTipText("Hapus Data");
        btnCetak.setToolTipText("Cetak Data");
        btnKeluar.setToolTipText("Keluar");
        txt_nama_jabatan.requestFocus();
                
        model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tbl_jabatan_fungsional.setModel(model);
        model.addColumn("Kode Jabatan");
        model.addColumn("Nama Jabatan Fungsional");
        model.addColumn("Tunjangan (Rp.)");  
        bacatabel();
    }
    public void bacatabel(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="select * from jab_fungsional";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[3];
                o[0]=r.getString("kd_jabatan");
                o[1]=r.getString("nama_jabatan");
                o[2]=r.getString("tunj_jabatan");
                model.addRow(o);
            }
            r.close();
            s.close();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }

    }
    void autonumber(){
        try{
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        Statement state = con.createStatement();
        String sql = "select max(kd_jabatan) from jab_fungsional";
        ResultSet rs = state.executeQuery(sql);
         while(rs.next()){
             int a = rs.getInt(1);
            txt_kode_jabatan.setText("00"+Integer.toString(a+1));
        rs.close();
        con.close();
        }}
        catch (Exception ex){
        
        }
    }
     
    public void TambahData(){
    String kode_jabatan=this.txt_kode_jabatan.getText();
    String nama_jabatan=this.txt_nama_jabatan.getText();
    String jumlah_tunjangan=this.txt_jumlah_tunjangan.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "INSERT INTO jab_fungsional VALUES (?,?,?)";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_jabatan);
            p.setString(2, nama_jabatan);
            p.setString(3, jumlah_tunjangan);
            p.executeUpdate();
            p.close();
            bersih();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}

public void UpdateData() {
    int i=tbl_jabatan_fungsional.getSelectedRow();
        if(i==-1)
        {
            return;
        }
    String kode_jabatan=(String) model.getValueAt(i, 0);
    String nama_jabatan=this.txt_nama_jabatan.getText();
    String jumlah_tunjangan=this.txt_jumlah_tunjangan.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE jab_fungsional Set nama_jabatan=?,tunj_jabatan=? WHERE kd_jabatan=?";
            PreparedStatement p=(PreparedStatement) c.prepareStatement(sql);
            
            p.setString(1, nama_jabatan);
            p.setString(2, jumlah_tunjangan);
            p.setString(3, kode_jabatan);
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}
public void DeleteData() {
    int i=tbl_jabatan_fungsional.getSelectedRow();
        if(i==-1)
        {
            return;
        }
        String kode_golongan=(String) model.getValueAt(i, 0);
       
       
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE From jab_fungsional  WHERE kd_jabatan=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_golongan);            
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println("Terjadi kesalahan");
        }finally{
            bacatabel();
        }
}
   

       private void bersih() {
        //txt_kode_jabatan.setText("");
        txt_nama_jabatan.setText("");
        txt_jumlah_tunjangan.setText("");
        txt_nama_jabatan.requestFocus();
       }
       
       private void settabel(){
       int kolom=tbl_jabatan_fungsional.getSelectedRow();
       txt_kode_jabatan.setText((String)tbl_jabatan_fungsional.getValueAt(kolom,0));
       txt_nama_jabatan.setText((String)tbl_jabatan_fungsional.getValueAt(kolom,1));
       txt_jumlah_tunjangan.setText((String)tbl_jabatan_fungsional.getValueAt(kolom,2));
       
       }

       void filternama(KeyEvent a){
        if(Character.isDigit(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Nama Jabatan'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
       
        void filterjabatan(KeyEvent b){
        if(Character.isAlphabetic(b.getKeyChar())){
            b.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Jumlah Tunjangan'> Hanya Bisa Memasukan Karakter Angka", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_jumlah_tunjangan = new javax.swing.JTextField();
        txt_nama_jabatan = new javax.swing.JTextField();
        txt_kode_jabatan = new javax.swing.JTextField();
        btnTambah = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_jabatan_fungsional = new javax.swing.JTable();
        btnCetak = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel5.setFont(new java.awt.Font("Bauhaus Md BT", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("MASTER DATA JABATAN FUNGSIONAL");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Kode Jabatan");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Nama Jabatan");

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Jumlah Tunjangan");

        txt_jumlah_tunjangan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_jumlah_tunjanganKeyTyped(evt);
            }
        });

        txt_nama_jabatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nama_jabatanKeyTyped(evt);
            }
        });

        txt_kode_jabatan.setEditable(false);

        btnTambah.setText("TAMBAH");
        btnTambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnUbah.setText("UBAH");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setText("HAPUS");
        btnHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        tbl_jabatan_fungsional.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_jabatan_fungsional.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_jabatan_fungsionalMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_jabatan_fungsional);

        btnCetak.setText("Cetak Data");
        btnCetak.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });

        btnKeluar.setText("Keluar");
        btnKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 657, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(177, 177, 177)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_nama_jabatan)
                                    .addComponent(txt_kode_jabatan)
                                    .addComponent(txt_jumlah_tunjangan, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnTambah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(22, 22, 22))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_kode_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTambah))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUbah)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnHapus))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txt_nama_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txt_jumlah_tunjangan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Masukkan data dengan benar !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        TambahData();
        bersih();
        autonumber();
        try{
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
        }
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau dirubah !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        UpdateData();
        bersih();
        autonumber();
        try{
            JOptionPane.showMessageDialog(null, "Data Berhasil Di Update");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Di Update");
        }
    }//GEN-LAST:event_btnUbahActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
         if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau di hapus !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Hapus Data Ini ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){

            String kode=(String) this.txt_kode_jabatan.getText();
            if ("".equals(kode.trim()) || kode.trim()==null)
            {
                return;
            }else{
                DeleteData();
                bersih();
                autonumber();
                try{
                    JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Data Gagal Dihapus");
                }}}
    }//GEN-LAST:event_btnHapusActionPerformed

    private void tbl_jabatan_fungsionalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_jabatan_fungsionalMouseClicked
        // TODO add your handling code here:
         settabel();
    }//GEN-LAST:event_tbl_jabatan_fungsionalMouseClicked

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
        // TODO add your handling code here:
         try { 
         
            File file = new File("src/Laporan/LaporanJabatanFungsional.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            parham.clear();
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport,parham,conn);
            JasperViewer.viewReport(jasperPrint, false);
            } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Data tidak dapat dicetak!"+ "\n" + e.getMessage(), "Cetak Data", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCetakActionPerformed

    private void txt_nama_jabatanKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nama_jabatanKeyTyped
        // TODO add your handling code here:
           filternama(evt);
    }//GEN-LAST:event_txt_nama_jabatanKeyTyped

    private void txt_jumlah_tunjanganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jumlah_tunjanganKeyTyped
        // TODO add your handling code here:
           filterjabatan(evt);
    }//GEN-LAST:event_txt_jumlah_tunjanganKeyTyped

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
        
    }//GEN-LAST:event_btnKeluarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatanFungsional.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatanFungsional.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatanFungsional.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatanFungsional.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MasterDataJabatanFungsional().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCetak;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnUbah;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_jabatan_fungsional;
    private javax.swing.JTextField txt_jumlah_tunjangan;
    private javax.swing.JTextField txt_kode_jabatan;
    private javax.swing.JTextField txt_nama_jabatan;
    // End of variables declaration//GEN-END:variables
}
