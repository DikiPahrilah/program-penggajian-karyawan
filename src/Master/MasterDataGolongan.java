/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Master;

import Koneksi.koneksi;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FachrealHeart
 */
public class MasterDataGolongan extends javax.swing.JFrame {
 private DefaultTableModel model;
    
JasperReport jasperReport; 
JasperDesign jasperDesign;
JasperPrint jasperPrint;

Map <String,Object> parham = new HashMap<String,Object>();
Connection conn = new koneksi().getKoneksi();
    /**
     * Creates new form MasterDataGolongan
     */
    public MasterDataGolongan() {
        initComponents();
        autonumber();
        txt_nama_pangkat.setToolTipText("Silahkan Masukkan nama pangkat disini");
        txt_golongan.setToolTipText("Silahkan Masukkan golongan / ruang karyawan");
        txt_jumlah_gaji_pokok.setToolTipText("Silahkan Masukkan besar jumlah gaji pokok");
        btnTambah.setToolTipText("Tambah Data");
        btnUbah.setToolTipText("Ubah Data");
        btnHapus.setToolTipText("Hapus Data");
        btnBersih.setToolTipText("Bersih Text");
        btnCetakData.setToolTipText("Cetak Data");
        btnKeluar.setToolTipText("Keluar");
        txt_nama_pangkat.requestFocus();
        
        model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tbl_data_golongan.setModel(model);
        model.addColumn("Kode Golongan");
        model.addColumn("Pangkat");
        model.addColumn("Golongan / Ruang");  
        model.addColumn("Gaji Pokok (Rp)");
        bacatabel();
    }
    public void bacatabel(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="Select * from pangkat_gol";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[4];
                o[0]=r.getString("kd_pangkat");
                o[1]=r.getString("nama_pangkat");
                o[2]=r.getString("golongan");
                o[3]=r.getString("jml_gapok");
                model.addRow(o);
            }
            r.close();
            s.close();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }

    }
    
     void autonumber(){
        try{
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        Statement state = con.createStatement();
        String sql = "select max(kd_pangkat) from pangkat_gol";
        ResultSet rs = state.executeQuery(sql);
         while(rs.next()){
             int a = rs.getInt(1);
        txt_kode_golongan.setText("00"+Integer.toString(a+1));
        rs.close();
        con.close();
        }}
        catch (Exception ex){
        
        }
    }
     
    public void TambahData(){
    String kode_golongan=this.txt_kode_golongan.getText();
    String nama_pangkat=this.txt_nama_pangkat.getText();
    String golongan=this.txt_golongan.getText() ;
    String jumlah_gaji_pokok=this.txt_jumlah_gaji_pokok.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "INSERT INTO pangkat_gol VALUES (?,?,?,?)";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_golongan);
            p.setString(2, nama_pangkat);
            p.setString(3, golongan);
            p.setString(4, jumlah_gaji_pokok);
            p.executeUpdate();
            p.close();
            bersih();
            autonumber();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}

public void UpdateData() {
    int i=tbl_data_golongan.getSelectedRow();
        if(i==-1)
        {
            return;
        }
    String kode_golongan=(String) model.getValueAt(i, 0);
    String nama_pangkat=this.txt_nama_pangkat.getText();
    String golongan=this.txt_golongan.getText() ;
    String jumlah_gaji_pokok=this.txt_jumlah_gaji_pokok.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE pangkat_gol Set nama_pangkat=?,golongan=?,jml_gapok=? WHERE kd_pangkat=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            
            p.setString(1, nama_pangkat);
            p.setString(2, golongan);
            p.setString(3, jumlah_gaji_pokok);
            p.setString(4, kode_golongan);
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}
public void DeleteData() {
    int i=tbl_data_golongan.getSelectedRow();
        if(i==-1)
        {
            return;
        }
        String kode_golongan=(String) model.getValueAt(i, 0);
       
       
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE From pangkat_gol  WHERE kd_pangkat=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_golongan);            
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println("Terjadi kesalahan");
        }finally{
            bacatabel();
        }
}
   

       private void bersih() {
        //txt_kode_golongan.setText("");
        txt_nama_pangkat.setText("");
        txt_golongan.setText("");
        txt_jumlah_gaji_pokok.setText("");
        txt_nama_pangkat.requestFocus();
       }
       
       private void settabel(){
       int kolom=tbl_data_golongan.getSelectedRow();
       txt_kode_golongan.setText((String)tbl_data_golongan.getValueAt(kolom,0));
       txt_nama_pangkat.setText((String)tbl_data_golongan.getValueAt(kolom,1));
       txt_golongan.setText((String)tbl_data_golongan.getValueAt(kolom,2));
       txt_jumlah_gaji_pokok.setText((String)tbl_data_golongan.getValueAt(kolom,3));
       
       }

       void filternama(KeyEvent a){
        if(Character.isDigit(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Nama Pangkat'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
       
        void filtergapok(KeyEvent b){
        if(Character.isAlphabetic(b.getKeyChar())){
            b.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Jumlah Gaji Pokok'> Hanya Bisa Memasukan Karakter Angka", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_kode_golongan = new javax.swing.JTextField();
        txt_nama_pangkat = new javax.swing.JTextField();
        txt_golongan = new javax.swing.JTextField();
        txt_jumlah_gaji_pokok = new javax.swing.JTextField();
        btnTambah = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_data_golongan = new javax.swing.JTable();
        btnCetakData = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();
        btnBersih = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel5.setFont(new java.awt.Font("Bauhaus Md BT", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("MASTER DATA GOLONGAN");

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Kode  Golongan");

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Nama Pangkat");

        jLabel9.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Golongan");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Jum. Gaji Pokok");

        txt_kode_golongan.setEditable(false);

        txt_nama_pangkat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nama_pangkatKeyTyped(evt);
            }
        });

        txt_golongan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_golonganKeyTyped(evt);
            }
        });

        txt_jumlah_gaji_pokok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_jumlah_gaji_pokokKeyTyped(evt);
            }
        });

        btnTambah.setText("TAMBAH");
        btnTambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnUbah.setText("UBAH");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setText("HAPUS");
        btnHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        tbl_data_golongan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_data_golongan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_data_golonganMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_data_golongan);

        btnCetakData.setText("Cetak Data");
        btnCetakData.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCetakData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakDataActionPerformed(evt);
            }
        });

        btnKeluar.setText("Keluar");
        btnKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });

        btnBersih.setText("BERSIH");
        btnBersih.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBersih.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBersihActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_kode_golongan, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(txt_nama_pangkat, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(txt_golongan, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(txt_jumlah_gaji_pokok, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnTambah, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                            .addComponent(btnUbah, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                            .addComponent(btnHapus, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                            .addComponent(btnBersih, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel5))
                .addGap(47, 47, 47))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(390, 390, 390)
                        .addComponent(btnCetakData, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(txt_kode_golongan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(txt_nama_pangkat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(txt_golongan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTambah)
                        .addGap(7, 7, 7)
                        .addComponent(btnUbah)
                        .addGap(7, 7, 7)
                        .addComponent(btnHapus)))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_jumlah_gaji_pokok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBersih)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCetakData, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
       if(txt_kode_golongan.getText().equals("")||txt_nama_pangkat.getText().equals("")||txt_golongan.getText().equals("")||txt_jumlah_gaji_pokok.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Masukkan data dengan benar !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        TambahData();
        bersih();
        try{
             JOptionPane.showMessageDialog(null,"Data berhasil disimpan","SUKSES",JOptionPane.INFORMATION_MESSAGE,new ImageIcon("src/Icon/save.png"));
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
        }
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        if(txt_kode_golongan.getText().equals("")||txt_nama_pangkat.getText().equals("")||txt_golongan.getText().equals("")||txt_jumlah_gaji_pokok.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau di ubah !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        UpdateData();
        bersih();
        autonumber();
        try{
            JOptionPane.showMessageDialog(null, "Data Berhasil Di Update");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Di Update");
        }
    }//GEN-LAST:event_btnUbahActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
         if(txt_kode_golongan.getText().equals("")||txt_nama_pangkat.getText().equals("")||txt_golongan.getText().equals("")||txt_jumlah_gaji_pokok.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau di hapus !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Hapus Data Ini ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){

            String kode=(String) this.txt_kode_golongan.getText();
            if ("".equals(kode.trim()) || kode.trim()==null)
            {
                return;
            }else{
                DeleteData();
                bersih();
                autonumber();
                try{
                    JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Data Gagal Dihapus");
                }}}
    }//GEN-LAST:event_btnHapusActionPerformed

    private void tbl_data_golonganMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_data_golonganMouseClicked
        // TODO add your handling code here:
         settabel();
    }//GEN-LAST:event_tbl_data_golonganMouseClicked

    private void btnCetakDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakDataActionPerformed
        // TODO add your handling code here:
        try { 
         
            File file = new File("src/Laporan/LaporanPangkatGolongan.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            parham.clear();
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport,parham,conn);
            JasperViewer.viewReport(jasperPrint, false);
            } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Data tidak dapat dicetak!"+ "\n" + e.getMessage(), "Cetak Data", JOptionPane.ERROR_MESSAGE);
} 
    }//GEN-LAST:event_btnCetakDataActionPerformed

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
        
    }//GEN-LAST:event_btnKeluarActionPerformed

    private void txt_nama_pangkatKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nama_pangkatKeyTyped
        // TODO add your handling code here:
        filternama(evt);
    }//GEN-LAST:event_txt_nama_pangkatKeyTyped

    private void txt_golonganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_golonganKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_golonganKeyTyped

    private void txt_jumlah_gaji_pokokKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jumlah_gaji_pokokKeyTyped
        // TODO add your handling code here:
        filtergapok(evt);
    }//GEN-LAST:event_txt_jumlah_gaji_pokokKeyTyped

    private void btnBersihActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBersihActionPerformed
        // TODO add your handling code here:
        bersih();
        autonumber();
    }//GEN-LAST:event_btnBersihActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterDataGolongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterDataGolongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterDataGolongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterDataGolongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MasterDataGolongan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBersih;
    private javax.swing.JButton btnCetakData;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnUbah;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_data_golongan;
    private javax.swing.JTextField txt_golongan;
    private javax.swing.JTextField txt_jumlah_gaji_pokok;
    private javax.swing.JTextField txt_kode_golongan;
    private javax.swing.JTextField txt_nama_pangkat;
    // End of variables declaration//GEN-END:variables
}
