/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Master;

import Koneksi.koneksi;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FachrealHeart
 */
public class MasterDataJabatan extends javax.swing.JFrame {
private DefaultTableModel model;

JasperReport jasperReport; 
JasperDesign jasperDesign;
JasperPrint jasperPrint;

Map <String,Object> parham = new HashMap<String,Object>();
Connection conn = new koneksi().getKoneksi();
    /**
     * Creates new form MasterDataJabatan
     */
    public MasterDataJabatan() {
        initComponents();
        autonumber();
        txt_nama_jabatan.setToolTipText("Silahkan Masukkan nama jabatan disini");
        txt_jumlah_tunjangan.setToolTipText("Silahkan Masukkan jumlah tunjangan karyawan");
        btnTambah.setToolTipText("Tambah Data");
        btnUbah.setToolTipText("Ubah Data");
        btnHapus.setToolTipText("Hapus Data");
        btnCetak.setToolTipText("Cetak Data");
        btnKeluar.setToolTipText("Keluar");
        txt_nama_jabatan.requestFocus();
                
        model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tbl_data_jabatan.setModel(model);
        model.addColumn("Kode Jabatan");
        model.addColumn("Nama Jabatan Struktural");
        model.addColumn("Tunjangan (Rp.)");  
        bacatabel();
    }
    public void bacatabel(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="select * from jab_struktural";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[3];
                o[0]=r.getString("kd_jab");
                o[1]=r.getString("nama_jab");
                o[2]=r.getString("tunj_jab");
                model.addRow(o);
            }
            r.close();
            s.close();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }

    }
    void autonumber(){
        try{
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        Statement state = con.createStatement();
        String sql = "select max(kd_jab) from jab_struktural";
        ResultSet rs = state.executeQuery(sql);
         while(rs.next()){
             int a = rs.getInt(1);
            txt_kode_jabatan.setText("00"+Integer.toString(a+1));
        rs.close();
        con.close();
        }}
        catch (Exception ex){
        
        }
    }
     
    public void TambahData(){
    String kode_jabatan=this.txt_kode_jabatan.getText();
    String nama_jabatan=this.txt_nama_jabatan.getText();
    String jumlah_tunjangan=this.txt_jumlah_tunjangan.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "INSERT INTO jab_struktural VALUES (?,?,?)";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_jabatan);
            p.setString(2, nama_jabatan);
            p.setString(3, jumlah_tunjangan);
            p.executeUpdate();
            p.close();
            bersih();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}

public void UpdateData() {
    int i=tbl_data_jabatan.getSelectedRow();
        if(i==-1)
        {
            return;
        }
    String kode_jabatan=(String) model.getValueAt(i, 0);
    String nama_jabatan=this.txt_nama_jabatan.getText();
    String jumlah_tunjangan=this.txt_jumlah_tunjangan.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE jab_struktural Set nama_jab=?,tunj_jab=? WHERE kd_jab=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
           
            p.setString(1, nama_jabatan);
            p.setString(2, jumlah_tunjangan);
            p.setString(3, kode_jabatan);
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}
public void DeleteData() {
    int i=tbl_data_jabatan.getSelectedRow();
        if(i==-1)
        {
            return;
        }
        String kode_golongan=(String) model.getValueAt(i, 0);
       
       
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE From jab_struktural  WHERE kd_jab=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, kode_golongan);            
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println("Terjadi kesalahan");
        }finally{
            bacatabel();
        }
}
   

       private void bersih() {
        //txt_kode_jabatan.setText("");
        txt_nama_jabatan.setText("");
        txt_jumlah_tunjangan.setText("");
        txt_nama_jabatan.requestFocus();
       }
       
       private void settabel(){
       int kolom=tbl_data_jabatan.getSelectedRow();
       txt_kode_jabatan.setText((String)tbl_data_jabatan.getValueAt(kolom,0));
       txt_nama_jabatan.setText((String)tbl_data_jabatan.getValueAt(kolom,1));
       txt_jumlah_tunjangan.setText((String)tbl_data_jabatan.getValueAt(kolom,2));
       
       }

       void filternama(KeyEvent a){
        if(Character.isDigit(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Nama Jabatan'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
       
        void filterjabatan(KeyEvent b){
        if(Character.isAlphabetic(b.getKeyChar())){
            b.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Jumlah Tunjangan'> Hanya Bisa Memasukan Karakter Angka", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_kode_jabatan = new javax.swing.JTextField();
        txt_nama_jabatan = new javax.swing.JTextField();
        txt_jumlah_tunjangan = new javax.swing.JTextField();
        btnTambah = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_data_jabatan = new javax.swing.JTable();
        btnCetak = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel5.setFont(new java.awt.Font("Bauhaus Md BT", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("MASTER DATA JABATAN STRUKTURAL");

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Kode Jabatan");

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Nama Jabatan");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Jumlah Tunjangan");

        txt_kode_jabatan.setEditable(false);

        txt_nama_jabatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nama_jabatanKeyTyped(evt);
            }
        });

        txt_jumlah_tunjangan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_jumlah_tunjanganKeyTyped(evt);
            }
        });

        btnTambah.setText("TAMBAH");
        btnTambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnUbah.setText("UBAH");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setText("HAPUS");
        btnHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        tbl_data_jabatan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_data_jabatan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_data_jabatanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_data_jabatan);

        btnCetak.setText("Cetak Data");
        btnCetak.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });

        btnKeluar.setText("Keluar");
        btnKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(txt_kode_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(txt_nama_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(txt_jumlah_tunjangan, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel5))
                .addGap(24, 24, 24))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 657, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTambah)
                        .addGap(7, 7, 7)
                        .addComponent(btnUbah)
                        .addGap(7, 7, 7)
                        .addComponent(btnHapus))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(txt_kode_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(txt_nama_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(txt_jumlah_tunjangan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
         if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Masukkan data dengan benar !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        TambahData();
        bersih();
        autonumber();
        try{
            JOptionPane.showMessageDialog(null,"Data berhasil disimpan","SUKSES",JOptionPane.INFORMATION_MESSAGE,new ImageIcon("src/Icon/save.png"));
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
        }
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
         if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau dirubah !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        UpdateData();
        bersih();
        autonumber();
        try{
            JOptionPane.showMessageDialog(null, "Data Berhasil Di Update");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Di Update");
        }
    }//GEN-LAST:event_btnUbahActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        if(txt_kode_jabatan.getText().equals("")||txt_nama_jabatan.getText().equals("")||txt_jumlah_tunjangan.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau di hapus !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Hapus Data Ini ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){

            String kode=(String) this.txt_kode_jabatan.getText();
            if ("".equals(kode.trim()) || kode.trim()==null)
            {
                return;
            }else{
                DeleteData();
                bersih();
                autonumber();
                try{
                    JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Data Gagal Dihapus");
                }}}
    }//GEN-LAST:event_btnHapusActionPerformed

    private void tbl_data_jabatanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_data_jabatanMouseClicked
        // TODO add your handling code here:
        settabel();
    }//GEN-LAST:event_tbl_data_jabatanMouseClicked

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
        // TODO add your handling code here:
        try { 
         
            File file = new File("src/Laporan/LaporanJabatanStruktural.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            parham.clear();
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport,parham,conn);
            JasperViewer.viewReport(jasperPrint, false);
            } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Data tidak dapat dicetak!"+ "\n" + e.getMessage(), "Cetak Data", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCetakActionPerformed

    private void txt_nama_jabatanKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nama_jabatanKeyTyped
        // TODO add your handling code here:
         filternama(evt);
    }//GEN-LAST:event_txt_nama_jabatanKeyTyped

    private void txt_jumlah_tunjanganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jumlah_tunjanganKeyTyped
        // TODO add your handling code here:
         filterjabatan(evt);
    }//GEN-LAST:event_txt_jumlah_tunjanganKeyTyped

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnKeluarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterDataJabatan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MasterDataJabatan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCetak;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnUbah;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_data_jabatan;
    private javax.swing.JTextField txt_jumlah_tunjangan;
    private javax.swing.JTextField txt_kode_jabatan;
    private javax.swing.JTextField txt_nama_jabatan;
    // End of variables declaration//GEN-END:variables
}
