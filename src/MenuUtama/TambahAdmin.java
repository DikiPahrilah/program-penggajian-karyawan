/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MenuUtama;

import Koneksi.koneksi;
import Login.LoginHash;
import java.awt.event.KeyEvent;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FachrealHeart
 */
public class TambahAdmin extends javax.swing.JFrame {
    private DefaultTableModel model;
    Connection c;
    ResultSet r;
    Statement s;

    /**
     * Creates new form TambahAdmin
     */
    public TambahAdmin() {
        initComponents();
         bukakoneksi();
        txt_nama_lengkap.setToolTipText("Masukkan <Nama Lengkap> disini");
        txt_username.setToolTipText("Masukkan <Username> disini");
        txt_password.setToolTipText("Masukkan <Password> disini");
        btn_ubah.setToolTipText("Ubah");
        btn_tambahkan.setToolTipText("Tambah");
        btn_hapus.setToolTipText("Hapus Data");
        btn_keluar.setToolTipText("Keluar");
        txt_nama_lengkap.requestFocus();
        
        model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        jTable1.setModel(model);
        model.addColumn("Nama Admin");
        model.addColumn("Username");
        model.addColumn("Password");   
        //ambiljabatan();
        bacatabel();
    }
public void bacatabel(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="Select * from login";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[3];
                o[0]=r.getString("nama_admin");
                o[1]=r.getString("username");
                o[2]=r.getString("password");
                model.addRow(o);
            }
            r.close();
            s.close();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }

    }

     private void bukakoneksi(){
    try {
        Class.forName("com.mysql.jdbc.Driver");
        c= DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        System.out.println("koneksi sukses");
    } catch (ClassNotFoundException | SQLException e) {
        System.out.println(e);
    }
   }

      
       private void simpan(){
       try {
           s=c.createStatement();
           String sql;
           sql = "insert into login values('"+txt_nama_lengkap.getText()+"','"+txt_username.getText()+"','"+txt_password.getText()+"')";
           s.executeUpdate(sql);
           s.close();
           
           JOptionPane.showMessageDialog(null,"Admin baru telah ditambahkan","SUKSES",JOptionPane.INFORMATION_MESSAGE,new ImageIcon("src/Icon/save.png"));
           bacatabel(); 
           bersih();
           
       } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,e);
       }
   }
 
 public void UpdateData() {
    int i=jTable1.getSelectedRow();
        if(i==-1)
        {
            return;
        }
    String nama_admin=(String) model.getValueAt(i, 0);
    String username=this.txt_username.getText() ;
    String password=this.txt_password.getText() ;
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE login Set username=?,password=? WHERE nama_admin=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
           
            p.setString(1, username);
            p.setString(2, password);
            p.setString(3, nama_admin);
            p.executeUpdate();
            p.close();
           
            JOptionPane.showMessageDialog(null, "Data Berhasil Di ubah");
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            bacatabel();
        }
}
public void DeleteData() {
    int i=jTable1.getSelectedRow();
        if(i==-1)
        {
            return;
        }
        String nama_admin=(String) model.getValueAt(i, 0);
       
       
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE From login  WHERE nama_admin=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, nama_admin);            
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println("Terjadi kesalahan");
        }finally{
            bacatabel();
        }
}
  
       private void bersih() {
        txt_nama_lengkap.setText("");
        txt_username.setText("");
        txt_password.setText("");
        txt_nama_lengkap.requestFocus();
       }
 
        private void settabel(){
       int kolom=jTable1.getSelectedRow();
       txt_nama_lengkap.setText((String)jTable1.getValueAt(kolom,0));
       txt_username.setText((String)jTable1.getValueAt(kolom,1));
       txt_password.setText((String)jTable1.getValueAt(kolom,2));
       }
        
   void filternama(KeyEvent a){
     if(Character.isDigit(a.getKeyChar())){
         a.consume();
         JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Nama Lengkap'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
     }
 }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        clGradientPanel1 = new GradientPanel.ClGradientPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btn_ubah = new GradientButton.ClGradientButton();
        btn_hapus = new GradientButton.ClGradientButton();
        btn_tambahkan = new GradientButton.ClGradientButton();
        btn_keluar = new GradientButton.ClGradientButton();
        txt_password = new javax.swing.JPasswordField();
        txt_username = new javax.swing.JTextField();
        txt_nama_lengkap = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 255, 102));
        jLabel2.setText("** Username tidak boleh sama");

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 255, 102));
        jLabel1.setText("Catatan :     * Hanya Admin Yang dapat Menambahkan Admin Baru");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 255, 204));
        jLabel4.setText("Nama Lengkap");

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 255, 204));
        jLabel5.setText("Username");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(204, 255, 204));
        jLabel6.setText("Password");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_tambahkan.setText("Tambahkan");
        btn_tambahkan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahkanActionPerformed(evt);
            }
        });

        btn_keluar.setText("Keluar");
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });

        txt_nama_lengkap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nama_lengkapKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout clGradientPanel1Layout = new javax.swing.GroupLayout(clGradientPanel1);
        clGradientPanel1.setLayout(clGradientPanel1Layout);
        clGradientPanel1Layout.setHorizontalGroup(
            clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(clGradientPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                                .addGap(64, 64, 64)
                                .addComponent(jLabel2))))
                    .addGroup(clGradientPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btn_hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_tambahkan, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_keluar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_password)
                                    .addComponent(txt_username)
                                    .addComponent(txt_nama_lengkap, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        clGradientPanel1Layout.setVerticalGroup(
            clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(clGradientPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(clGradientPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addGap(30, 30, 30)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(clGradientPanel1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txt_nama_lengkap, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                            .addComponent(txt_username))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_tambahkan, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(clGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_keluar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(clGradientPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(clGradientPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        settabel(); // TODO add your handling code here:
    }//GEN-LAST:event_jTable1MouseClicked

    private void btn_tambahkanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahkanActionPerformed
        if(txt_nama_lengkap.getText().equals("")||txt_username.getText().equals("")||txt_password.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Masukkan data dengan benar !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        simpan();
        txt_nama_lengkap.requestFocus();
        /**
        try {
        Class.forName("com.mysql.jdbc.Driver");
        c= DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        
        PreparedStatement state = c.prepareStatement("INSERT INTO login VALUES (?,?,?)");
        
        state.setString(1, txt_nama_lengkap.getText());
        state.setString(2, txt_username.getText());
        
        if(md5(txt_password.getPassword()).equals(""))
        {
            JOptionPane.showMessageDialog(this, "Can not encrypted", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
        }
        state.setString(3, md5(txt_password.getPassword()));
        
        int count = state.executeUpdate();
        if (count > 0){
            JOptionPane.showMessageDialog(null,"Admin baru telah ditambahkan","SUKSES",JOptionPane.INFORMATION_MESSAGE,new ImageIcon("src/Icons/save.png"));
           bacatabel(); 
           bersih();
        }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "Error raise", JOptionPane.ERROR_MESSAGE);
        }
       **/
    }//GEN-LAST:event_btn_tambahkanActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
    if(txt_nama_lengkap.getText().equals("")||txt_username.getText().equals("")||txt_password.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau diubah !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
    UpdateData();
    bersih();
    txt_nama_lengkap.requestFocus();
        /**
        try {
        Class.forName("com.mysql.jdbc.Driver");
        c= DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpenggajian","root","");
        
       String sql = "UPDATE login Set nama_admin=?,password=? WHERE username=?";
       com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
         //String sql = "UPDATE login Set nama_admin=?,username=?,password=? WHERE username=?";
        p.setString(1, txt_nama_lengkap.getText());
        p.setString(3, txt_username.getText());
        
        if(md5(txt_password.getPassword()).equals(""))
        {
            JOptionPane.showMessageDialog(this, "Can not encrypted", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
        }
        p.setString(2, md5(txt_password.getPassword()));
        
        int count = p.executeUpdate();
        if (count > 0){
            JOptionPane.showMessageDialog(null,"Admin berhasil dirubah","SUKSES",JOptionPane.INFORMATION_MESSAGE,new ImageIcon("src/Icons/save.png"));
             bacatabel();
        }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "Error raise", JOptionPane.ERROR_MESSAGE);
        }
        **/
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        if(txt_nama_lengkap.getText().equals("")||txt_username.getText().equals("")||txt_password.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih data yang mau dihapus !!!","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Hapus Data Ini ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){

            String nama=(String) this.txt_nama_lengkap.getText();
            if ("".equals(nama.trim()) || nama.trim()==null)
            {
                return;
            }else{
                DeleteData();
                txt_nama_lengkap.requestFocus();
                bersih();
                try{
                    JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Data Gagal Dihapus");
           }}}
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
        
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void txt_nama_lengkapKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nama_lengkapKeyTyped
        // TODO add your handling code here:
        filternama(evt);
    }//GEN-LAST:event_txt_nama_lengkapKeyTyped
    /**
     private String md5(char[] c){
        try{
            MessageDigest digs = MessageDigest.getInstance("MD5");
            
            digs.update((new String(c)).getBytes("UTF-8"));
            
            String str = new String(digs.digest());
            return str;
        } catch (Exception ex){
            return "";
        }
    }
    * /**
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TambahAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TambahAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TambahAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TambahAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TambahAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private GradientButton.ClGradientButton btn_hapus;
    private GradientButton.ClGradientButton btn_keluar;
    private GradientButton.ClGradientButton btn_tambahkan;
    private GradientButton.ClGradientButton btn_ubah;
    private GradientPanel.ClGradientPanel clGradientPanel1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txt_nama_lengkap;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextField txt_username;
    // End of variables declaration//GEN-END:variables
}
