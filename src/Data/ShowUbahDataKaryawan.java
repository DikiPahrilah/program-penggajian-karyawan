/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Koneksi.koneksi;
import com.mysql.jdbc.PreparedStatement;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author FachrealHeart
 */
public class ShowUbahDataKaryawan extends javax.swing.JFrame {
    ResultSet r;
    Statement s;
    Connection conn = new koneksi().getKoneksi();
    /**
     * Creates new form DataKaryawan
     */
    public ShowUbahDataKaryawan() {
        initComponents();
        txt_nama_karyawan.setToolTipText("Masukkan Nama Karyawan Disini, Maks.32 Karakter termasuk Spasi");
        txt_ttl.setToolTipText("Masukkan Tempat Lahir Karyawan Contoh : < BANDUNG >");
        txt_alamat.setToolTipText("Masukkan Alamat Karyawan dengan benar.");
        btnUbah.setToolTipText("Ubah Data");
        btnHapus.setToolTipText("Hapus Data");
        btnKeluar.setToolTipText("Keluar");
        autonumber();
        tampil_pangkat();
        tampil_jab_struk();
        tampil_jab_fungsional();
        tampil_status ();
        tampil_pendidikan ();
        tampil_anak();
        txt_nip.requestFocus();
    }
    
    void autonumber(){
        try{
        s  = conn.createStatement();
         String sql = "select max(nomor) from data_karyawan";
        ResultSet rs = s.executeQuery(sql);
         while(rs.next()){
             int a = rs.getInt(1);
            txt_no_urut.setText("00"+Integer.toString(a+1));
       
        }}
        catch (Exception ex){
        
        }
        }
    
          private void tampil_pangkat (){
         comboPangkatGol.addItem("-- Pilih Pangkat / Gol. --");
          try {
            String sql = "select golongan from pangkat_gol";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboPangkatGol.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
      
    
     private void tampil_jab_struk (){
         comboJabatanStruk.addItem("-- Pilih Jabatan Struktural --");
          try {
             String sql = "select nama_jab from jab_struktural";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboJabatanStruk.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
     
    
     private void tampil_jab_fungsional (){
         comboJabatanFungsi.addItem("-- Pilih Jabatan Fungsional --");
          try {
            String sql = "select nama_jabatan from jab_fungsional";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboJabatanFungsi.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
     
      private void tampil_pendidikan (){
         comboPendidikan.addItem("-- Pilih Pendidikan Terakhir --");
          try {
            String sql = "select jenjang_pend from pendidikan";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboPendidikan.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
      
     private void tampil_status (){
         comboStatus.addItem("-- Pilih Status Perkawinan --");
          try {
            String sql = "select status from status_kawin";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboStatus.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
     
          private void tampil_anak (){
         comboJumlahAnak.addItem("-- Jumlah Anak --");
          try {
            String sql = "select jumlah_anak from tunj_anak";
            Statement stat = conn.createStatement();
            ResultSet res=stat.executeQuery(sql);
            while (res.next()) {
                comboJumlahAnak.addItem(res.getString(1));
            }

        } catch (Exception e) {
        }
     }
     
     
    public void HapusText() {
    //txt_no_urut.setText("");
    //txt_nip.setText("");
    txt_nama_karyawan.setText("");
    txt_ttl.setText("");
    jDateChooser1.setDate(null);
    txt_alamat.setText("");
    comboPangkatGol.setSelectedIndex(0);
    comboJabatanStruk.setSelectedIndex(0);
    comboJabatanFungsi.setSelectedIndex(0);
    comboPendidikan.setSelectedIndex(0);
    comboStatus.setSelectedIndex(0);
    comboJumlahAnak.setSelectedIndex(0);

    }
    
  
   
 public void UpdateData() {

    String no=this.txt_no_urut.getText();
    String nip=this.txt_nip.getText();
    String nama=this.txt_nama_karyawan.getText() ;
    String tempat_lahir=this.txt_ttl.getText() ;
    java.util.Date tgl=(java.util.Date) this.jDateChooser1.getDate();
    String alamat=this.txt_alamat.getText();
    String golongan=(String) this.comboPangkatGol.getSelectedItem();
    String jabstruk=(String) this.comboJabatanStruk.getSelectedItem();
    String jabfungsi=(String) this.comboJabatanFungsi.getSelectedItem();
    String pendidikan=(String) this.comboPendidikan.getSelectedItem();
    String status=(String) this.comboStatus.getSelectedItem();
    String jml_anak=(String) this.comboJumlahAnak.getSelectedItem();
    
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE data_karyawan Set nip=?,nama=?,tempat_lahir=?,tgl_lahir=?,alamat=?,golongan=?,jab_struktur=?,jab_fungsional=?,pend_akhir=?,status_kawin=?,jumlah_anak=? WHERE nomor=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            
            p.setString(1, nip);
            p.setString(2, nama);
            p.setString(3, tempat_lahir);
            p.setDate(4,  new java.sql.Date(tgl.getTime()));
            p.setString(5, alamat);
            p.setString(6, golongan);
            p.setString(7, jabstruk);
            p.setString(8, jabfungsi);
            p.setString(9, pendidikan);
            p.setString(10, status);
            p.setString(11, jml_anak);
            p.setString(12, no);
            p.executeUpdate();
            p.close();
            JOptionPane.showMessageDialog(null,"Data telah dirubah");
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            //bacatabel();
        }
} 

  private void HapusData() {
       String no=this.txt_no_urut.getText();
       try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE From data_karyawan  WHERE no=?";
            PreparedStatement p=(PreparedStatement) c.prepareStatement(sql);
            p.setString(1, no);  
            p.executeUpdate();
            p.close();
           JOptionPane.showMessageDialog(null,"Data telah terhapus");
           HapusText();
       } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,e);
       }
   } 
 
void filternip(KeyEvent a){
        if(Character.isAlphabetic(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'NIP'> Hanya Bisa Memasukan Karakter Angka", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
    
 void filternama(KeyEvent b){
        if(Character.isDigit(b.getKeyChar())){
            b.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Nama Karyawan'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
 
 void filtertempat(KeyEvent c){
        if(Character.isDigit(c.getKeyChar())){
            c.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Tempat ...'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
 
 void filteralamat(KeyEvent d){
        if(Character.isDigit(d.getKeyChar())){
            d.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'Alamat'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_no_urut = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txt_nip = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_nama_karyawan = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_ttl = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_alamat = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        comboPangkatGol = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        comboJabatanStruk = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        comboJabatanFungsi = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        comboPendidikan = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        comboStatus = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        comboJumlahAnak = new javax.swing.JComboBox();
        Panel_glass_button = new usu.widget.glass.PanelGlass();
        btnUbah = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel26.setFont(new java.awt.Font("Century Gothic", 1, 36)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("UPDATE DATA KARYAWAN");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 153));
        jLabel4.setText("Nomor Urut");

        txt_no_urut.setEditable(false);

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 153));
        jLabel1.setText("N.I.P");

        txt_nip.setEditable(false);
        txt_nip.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nipKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 153));
        jLabel2.setText("Nama Karyawan");

        txt_nama_karyawan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_nama_karyawanKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 153));
        jLabel5.setText("Tempat, Tanggal Lahir");

        txt_ttl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_ttlKeyTyped(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 153));
        jLabel11.setText("Alamat Lengkap");

        txt_alamat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_alamatKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 153));
        jLabel3.setText("Pangkat/ Golongan");

        jLabel6.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 153));
        jLabel6.setText("Jabatan Struktural");

        jLabel7.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 153));
        jLabel7.setText("Jabatan Fungsional");

        jLabel8.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 153));
        jLabel8.setText("Pendidikan Terakhir");

        jLabel9.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 153));
        jLabel9.setText("Status Perkawinan");

        jLabel10.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 153));
        jLabel10.setText("Jumlah Anak");

        btnUbah.setText("Ubah");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnKeluar.setText("Keluar");
        btnKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });

        btnHapus.setText("Hapus");
        btnHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Panel_glass_buttonLayout = new javax.swing.GroupLayout(Panel_glass_button);
        Panel_glass_button.setLayout(Panel_glass_buttonLayout);
        Panel_glass_buttonLayout.setHorizontalGroup(
            Panel_glass_buttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel_glass_buttonLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );
        Panel_glass_buttonLayout.setVerticalGroup(
            Panel_glass_buttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel_glass_buttonLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(Panel_glass_buttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jDateChooser1.setDateFormatString("d MMM, yyyy");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel26)
                .addGap(95, 95, 95))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panel_glass_button, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(83, 83, 83)
                            .addComponent(txt_no_urut, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(34, 34, 34)
                            .addComponent(comboPangkatGol, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addGap(41, 41, 41)
                            .addComponent(comboJabatanStruk, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel7)
                            .addGap(35, 35, 35)
                            .addComponent(comboJabatanFungsi, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addGap(37, 37, 37)
                            .addComponent(comboPendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(42, 42, 42)
                            .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel10)
                            .addGap(75, 75, 75)
                            .addComponent(comboJumlahAnak, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addGap(52, 52, 52)
                                    .addComponent(txt_alamat))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(125, 125, 125)
                                    .addComponent(txt_nip)))
                            .addGap(50, 50, 50))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel2))
                            .addGap(21, 21, 21)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(txt_ttl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txt_nama_karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 81, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_no_urut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nama_karyawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_ttl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_alamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboPangkatGol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboJabatanStruk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboJabatanFungsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboPendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(comboJumlahAnak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Panel_glass_button, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
            if(txt_no_urut.getText().equals("")||txt_nip.getText().equals("")||txt_nama_karyawan.getText().equals("")||txt_ttl.getText().equals("")
               ||jDateChooser1.getDate().equals("")||txt_alamat.getText().equals("")||comboPangkatGol.getSelectedItem().equals("")
               ||comboJabatanStruk.getSelectedItem().equals("")||comboJabatanFungsi.getSelectedItem().equals("")||comboPendidikan.getSelectedItem().equals("")
               ||comboStatus.getSelectedItem().equals("")||comboJumlahAnak.getSelectedItem().equals(""))
            {
            JOptionPane.showMessageDialog(null, "Masukkan data dengan benar !!!","Message", JOptionPane.ERROR_MESSAGE);
                 return;
            } else
           {
        }
        UpdateData();
        autonumber();
        HapusText();
        //this.dispose();
    }//GEN-LAST:event_btnUbahActionPerformed

    private void txt_nipKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nipKeyTyped
        // TODO add your handling code here:
        filternip(evt);
    }//GEN-LAST:event_txt_nipKeyTyped

    private void txt_nama_karyawanKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nama_karyawanKeyTyped
        // TODO add your handling code here:
        filternama(evt);
    }//GEN-LAST:event_txt_nama_karyawanKeyTyped

    private void txt_ttlKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_ttlKeyTyped
        // TODO add your handling code here:
        filtertempat(evt);
    }//GEN-LAST:event_txt_ttlKeyTyped

    private void txt_alamatKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_alamatKeyTyped
        // TODO add your handling code here:
        filteralamat(evt);
    }//GEN-LAST:event_txt_alamatKeyTyped

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnKeluarActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        HapusData();
    }//GEN-LAST:event_btnHapusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowUbahDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowUbahDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowUbahDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowUbahDataKaryawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowUbahDataKaryawan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private usu.widget.glass.PanelGlass Panel_glass_button;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnUbah;
    public javax.swing.JComboBox comboJabatanFungsi;
    public javax.swing.JComboBox comboJabatanStruk;
    public javax.swing.JComboBox comboJumlahAnak;
    public javax.swing.JComboBox comboPangkatGol;
    public javax.swing.JComboBox comboPendidikan;
    public javax.swing.JComboBox comboStatus;
    public com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JTextField txt_alamat;
    public javax.swing.JTextField txt_nama_karyawan;
    public javax.swing.JTextField txt_nip;
    public javax.swing.JTextField txt_no_urut;
    public javax.swing.JTextField txt_ttl;
    // End of variables declaration//GEN-END:variables
}
