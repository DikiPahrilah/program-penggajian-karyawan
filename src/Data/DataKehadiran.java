/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Koneksi.koneksi;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fachreal Heart
 */
public class DataKehadiran extends javax.swing.JFrame {
     private DefaultTableModel model;
     

    /**
     * Creates new form FrmKehadiran
     */
    public DataKehadiran() {
        initComponents();
        loadcombodata();
        autonomor();
        
         model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tblKehadiran.setModel(model);
        model.addColumn("No.");
        model.addColumn("Nip");
        model.addColumn("Nama");
        model.addColumn("Jabatan");
        model.addColumn("Hadir");
        model.addColumn("Sakit"); 
        model.addColumn("Izin"); 
        model.addColumn("Tanpa Keterangan"); 
        model.addColumn("Bulan");
        model.addColumn("Tahun");
        model.addColumn("Total Lembur");
        loadData();
    }
    public void loadData(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="Select * from data_kehadiran";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[11];
                o[0]=r.getString("id_absen");
                o[1]=r.getString("nip");
                o[2]=r.getString("nama");
                o[3]=r.getString("nama_jab");
                o[4]=r.getString("hadir");
                o[5]=r.getString("sakit");                
                o[6]=r.getString("izin");  
                o[7]=r.getString("tanpa_ket"); 
                o[8]=r.getString("bulan"); 
                o[9]=r.getString("tahun"); 
                o[10]=r.getString("total_lembur");
                model.addRow(o);
            }
            r.close();
            s.close();
            ShowData();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }
    }
    private void autonomor(){
        String sql = "SELECT MAX(id_absen) FROM data_kehadiran";
        try{
           Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            ResultSet r=s.executeQuery(sql);
             while(r.next()){
                 int a = r.getInt(1);
                 txt_id_absen.setText(""+ Integer.toString(a+1));
                  this.txt_id_absen.enable(false);
             }
        }catch(Exception e){
            System.out.println(""+e.getMessage());
        }
    }
    
    //menampilkan ke feild dan comboBox sec, otomatis dari tabel data_denpoo
    public void loadcombodata(){
    try {        
        comboNip.removeAllItems();
        
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="SELECT * FROM data_karyawan ORDER BY nip ASC";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                comboNip.addItem(r.getString("nip"));
            }
            r.close();
            s.close();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }    
    }
    
    //untuk menambah / menginput ke database
    public void TambahData() {
    String id=this.txt_id_absen.getText();
    String nip=(String) this.comboNip.getSelectedItem();
    String nama=this.txt_nama.getText();
   
        String jabatan=this.txt_jabatan.getText();
        String hadir=this.txt_hadir.getText();
        String sakit=this.txt_sakit.getText();
        String izin=this.txt_izin.getText();
        String tanpa_ket=this.txt_tanpa_ket.getText();
        String bulan = this.comboBulan.getSelectedItem().toString();
        int tahun = this.YearChooser.getYear();
        int total_lembur = Integer.parseInt(txttotallembur.getText().toString());
        
         try {
            Connection c=koneksi.getKoneksi();
            String sql = "INSERT INTO data_kehadiran VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, id);
            p.setString(2, nip);
            p.setString(3, nama);
            p.setString(4, jabatan);
            p.setString(5, hadir);
            p.setString(6, sakit);  
            p.setString(7, izin);
            p.setString(8, tanpa_ket);
            p.setString(9, bulan);
            p.setInt(10, tahun);
            p.setInt(11, total_lembur);
            p.executeUpdate();
            p.close();
            //HapusText();
        }catch(SQLException e){
            //JOptionPane.showMessageDialog(rootPane, "Maaf Id Absen Tidak Boleh Sama !!!", "Edit Lagi", JOptionPane.WARNING_MESSAGE);
            System.out.println(e);
        }finally{
            loadData();
        }
    }
    private void reset_text(){
        //txt_id_absen.setText("");
        comboNip.setSelectedItem("");
        txt_nama.setText("");
       txt_jabatan.setText("");
        txt_hadir.setText("0");
        txt_izin.setText("0");
        txt_sakit.setText("0");
        txt_tanpa_ket.setText("0");
        txttotallembur.setText("");
    
    //progress
        setLocationRelativeTo(null);
        lblwait.setVisible(false);
        ProgressBar.setVisible(false);
    }
//buat tampil progress bar
   class TampilWorker extends SwingWorker{

        @Override
        protected Object doInBackground() throws Exception {
            for (int i = 0; i <= 100; i++) {
                try {
                    Thread.sleep(30);
                    ProgressBar.setVisible(true);
                    lblwait.setVisible(true);
                    ProgressBar.setValue(i);
                    ProgressBar.setIndeterminate(true);
                } catch (Exception e) {
                }
            }
            return null;
        } 
        @Override
        protected void done(){
            String id = txt_id_absen.getText();
            String nip = (String) comboNip.getSelectedItem();
            String nama = txt_nama.getText();
            String jabatan = txt_jabatan.getText();
            String hadir = txt_hadir.getText();
            String izin = txt_izin.getText();
            String sakit = txt_sakit.getText();
            String tanpa_ket = txt_tanpa_ket.getText();
            ProgressBar.setVisible(false);
            lblwait.setVisible(false);
            //JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
            reset_text();
        }
   }
   public void mouseClicked(MouseEvent e) {
      ShowData();
   }
    public void ShowData() {
   
    int i=tblKehadiran.getSelectedRow();
        
        if(i==-1)
        {
            return;
        }
        //String id=(String) model.getValueAt(i, 0);
        //txt_id_absen.setText(id);
        String nip=(String) model.getValueAt(i, 1);
        comboNip.setSelectedItem(nip);
        String nama=(String) model.getValueAt(i, 2);
        txt_nama.setText(nama);
        String jabatan = (String) model.getValueAt(i, 3);
        txt_jabatan.setText(jabatan);
        String hadir=(String) model.getValueAt(i, 4);
        txt_hadir.setText(hadir);
        String sakit=(String) model.getValueAt(i, 5);
        txt_sakit.setText(sakit);
        String izin=(String) model.getValueAt(i, 6);
        txt_izin.setText(izin);
        String tanpa_ket=(String) model.getValueAt(i, 7);
        txt_tanpa_ket.setText(tanpa_ket);
        String bulan=(String) model.getValueAt(i, 8);
        comboBulan.setSelectedItem(bulan);
        String tahun=(String) model.getValueAt(i, 7);
        txt_tanpa_ket.setText(tahun);
}
    
     void filternama(KeyEvent a){
        if(Character.isDigit(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'NAMA'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboNip = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_hadir = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_sakit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_izin = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_tanpa_ket = new javax.swing.JTextField();
        btnSimpan = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblKehadiran = new javax.swing.JTable();
        btnKembali = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txt_id_absen = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        lblwait = new javax.swing.JLabel();
        ProgressBar = new javax.swing.JProgressBar();
        jLabel9 = new javax.swing.JLabel();
        comboBulan = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        YearChooser = new com.toedter.calendar.JYearChooser();
        txt_jabatan = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txttotallembur = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kehadiran");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nip");

        comboNip.setEditable(true);
        comboNip.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboNip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboNipActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nama");

        txt_nama.setEditable(false);
        txt_nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_namaKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Jabatan");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Hadir");

        txt_hadir.setText("0");
        txt_hadir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_hadirKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_hadirKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Sakit");

        txt_sakit.setText("0");
        txt_sakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_sakitKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_sakitKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Izin");

        txt_izin.setText("0");
        txt_izin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_izinKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_izinKeyTyped(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Tanpa Keterangan");

        txt_tanpa_ket.setText("0");
        txt_tanpa_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_tanpa_ketKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_tanpa_ketKeyTyped(evt);
            }
        });

        btnSimpan.setText("Simpan");
        btnSimpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnReset.setText("Bersih");
        btnReset.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        tblKehadiran.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblKehadiran.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblKehadiranMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblKehadiran);

        btnKembali.setText("Keluar");
        btnKembali.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("No");

        txt_id_absen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_id_absenKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_id_absenKeyTyped(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        lblwait.setText("Please wait....");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(167, Short.MAX_VALUE)
                .addComponent(lblwait)
                .addGap(152, 152, 152))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblwait, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Bulan");

        comboBulan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" }));
        comboBulan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Tahun");

        txt_jabatan.setEditable(false);

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Total Lembur");

        txttotallembur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txttotallemburKeyTyped(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Jam");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txt_hadir, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txt_sakit, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(txt_izin, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txt_tanpa_ket, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(btnKembali, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txt_id_absen, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboNip, javax.swing.GroupLayout.Alignment.LEADING, 0, 188, Short.MAX_VALUE)
                            .addComponent(txt_nama, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_jabatan))
                        .addGap(109, 109, 109)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addGap(149, 149, 149)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(comboBulan, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(YearChooser, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txttotallembur, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel12)))))))
                .addContainerGap(168, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_id_absen, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(comboNip, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(comboBulan, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel10)
                            .addComponent(YearChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txttotallembur, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txt_jabatan, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_hadir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(txt_sakit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txt_izin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txt_tanpa_ket, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKembali, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void comboNipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboNipActionPerformed
        // TODO add your handling code here:
        int i=comboNip.getSelectedIndex();
        if (i==-1)
        {
            return;
        }
        try {
            String nm=(String) comboNip.getSelectedItem();
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql = "SELECT jab_struktur, nama FROM data_karyawan WHERE nip=?";
            PreparedStatement p=(PreparedStatement) c.prepareStatement(sql);
            p.setString(1, nm);
            ResultSet result=p.executeQuery();
            result.next();
            txt_nama.setText(result.getString("nama"));
             txt_jabatan.setText(result.getString("jab_struktur"));

        }catch(SQLException e){
            System.out.println(e);
        }
    }//GEN-LAST:event_comboNipActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        if(txt_id_absen.getText().equals("")||comboNip.getSelectedItem().equals("")||txt_nama.getText().equals("")||txt_hadir.getText().equals("")||txt_izin.getText().equals("")||txt_sakit.getText().equals("")||txt_tanpa_ket.getText().equals("")||comboBulan.getSelectedItem().equals("")||txttotallembur.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Masukkan data dengan benar","Message", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
        TampilWorker tampilWorker = new TampilWorker();//show progress bar
        TambahData();
        autonomor();
        reset_text();
        try{
                JOptionPane.showMessageDialog(null, "Data Kehadiran Berhasil Disimpan");
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
      }
           
            tampilWorker.execute();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void txt_hadirKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_hadirKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Hadir'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_hadirKeyTyped

    private void txt_sakitKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_sakitKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Sakit'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_sakitKeyTyped

    private void txt_izinKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_izinKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Izin'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_izinKeyTyped

    private void txt_tanpa_ketKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_tanpa_ketKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Tanpa Keterangan'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_tanpa_ketKeyTyped

    private void txt_id_absenKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_id_absenKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Id Absen'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_id_absenKeyTyped

    private void txt_id_absenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_id_absenKeyReleased
        // TODO add your handling code here:
         String input=txt_id_absen.getText();
        if(input.length()>11){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Id Absen'> max hanya 11 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_id_absen.setText("");
        }
    }//GEN-LAST:event_txt_id_absenKeyReleased

    private void tblKehadiranMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblKehadiranMouseClicked
        // TODO add your handling code here:
        ShowData();
        
    }//GEN-LAST:event_tblKehadiranMouseClicked

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void txt_namaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_namaKeyTyped
        // TODO add your handling code here:
        filternama(evt);
    }//GEN-LAST:event_txt_namaKeyTyped

    private void txt_hadirKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_hadirKeyReleased
        // TODO add your handling code here:
        String input=txt_hadir.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Hadir'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_hadir.setText("");
        }
        //
        validasino(evt);
    }//GEN-LAST:event_txt_hadirKeyReleased

    private void txt_sakitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_sakitKeyReleased
        // TODO add your handling code here:
         String input=txt_sakit.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Sakit'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_sakit.setText("");
        }
        //
         validasino(evt);
    }//GEN-LAST:event_txt_sakitKeyReleased

    private void txt_izinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_izinKeyReleased
        // TODO add your handling code here:
         String input=txt_izin.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Izin'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_izin.setText("");
        }
        
        //
         validasino(evt);
    }//GEN-LAST:event_txt_izinKeyReleased

    private void txt_tanpa_ketKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_tanpa_ketKeyReleased
        // TODO add your handling code here:
         String input=txt_tanpa_ket.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Tanpa Keterangan'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_tanpa_ket.setText("");
        }
        
        //
         validasino(evt);
    }//GEN-LAST:event_txt_tanpa_ketKeyReleased

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        // TODO add your handling code here:
        reset_text();
    }//GEN-LAST:event_btnResetActionPerformed

    private void txttotallemburKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttotallemburKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Total Lembur'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txttotallemburKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataKehadiran().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar ProgressBar;
    private com.toedter.calendar.JYearChooser YearChooser;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JComboBox comboBulan;
    private javax.swing.JComboBox comboNip;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblwait;
    private javax.swing.JTable tblKehadiran;
    private javax.swing.JTextField txt_hadir;
    private javax.swing.JTextField txt_id_absen;
    private javax.swing.JTextField txt_izin;
    private javax.swing.JTextField txt_jabatan;
    private javax.swing.JTextField txt_nama;
    private javax.swing.JTextField txt_sakit;
    private javax.swing.JTextField txt_tanpa_ket;
    private javax.swing.JTextField txttotallembur;
    // End of variables declaration//GEN-END:variables
      
     public void validasino(KeyEvent a){
         int hdr=0, skt=0, izn=0, tanpaket=0;
         if(txt_hadir.getText().equals("")){
             hdr=0;
         }else{
             hdr=Integer.parseInt(txt_hadir.getText());
         }
         
         if(txt_sakit.getText().equals("")){
             skt=0;
         }else{
             skt=Integer.parseInt(txt_sakit.getText());
         }
         
         if(txt_izin.getText().equals("")){
             izn=0;
         }else{
            izn=Integer.parseInt(txt_izin.getText());
         }
         
         if(txt_tanpa_ket.getText().equals("")){
             tanpaket=0;
         }else{
             tanpaket=Integer.parseInt(txt_tanpa_ket.getText());
         }
         int jml=hdr+skt+izn+tanpaket;
         System.out.println(jml);
         if (jml>25){
             a.consume();
             txt_hadir.setText("0");
             txt_sakit.setText("0");
             txt_izin.setText("0");
             txt_tanpa_ket.setText("0");
             JOptionPane.showMessageDialog(rootPane,"Hanya Bisa Menginputkan Kehadiran Sebesar 25 Hari", "Warning", JOptionPane.WARNING_MESSAGE);
         }
     }
}
