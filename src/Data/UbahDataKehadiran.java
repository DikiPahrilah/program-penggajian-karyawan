/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Koneksi.koneksi;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fachreal Heart
 */
public class UbahDataKehadiran extends javax.swing.JFrame {
     private DefaultTableModel model;

    /**
     * Creates new form FrmKehadiran
     */
    public UbahDataKehadiran() {
        initComponents();
        
         model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tblKehadiran.setModel(model);
        model.addColumn("No.");
        model.addColumn("Nip");
        model.addColumn("Nama");
        model.addColumn("Jabatan");
        model.addColumn("Hadir");
        model.addColumn("Sakit"); 
        model.addColumn("Izin"); 
        model.addColumn("Tanpa Keterangan");
        model.addColumn("Bulan");
        model.addColumn("Tahun");
        model.addColumn("Total Lembur");
        
        loadData();
    }
    public void loadData(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            Connection c=koneksi.getKoneksi();
            Statement s= c.createStatement();
            String sql="Select * from data_kehadiran";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[11];
                o[0]=r.getString("id_absen");
                o[1]=r.getString("nip");
                o[2]=r.getString("nama");
                o[3]=r.getString("nama_jab");
                o[4]=r.getString("hadir");
                o[5]=r.getString("sakit");                
                o[6]=r.getString("izin");  
                o[7]=r.getString("tanpa_ket"); 
                o[8]=r.getString("bulan");
                o[9]=r.getString("tahun");
                o[10]=r.getString("total_lembur");
                model.addRow(o);
            }
            r.close();
            s.close();
            ShowData();
        }catch(SQLException e) {
            System.out.println("Terjadi kesalahan");
        }
    }
    
 public void UpdateData() {
    int i=tblKehadiran.getSelectedRow();
       if(i==-1)
        {
            return;
     }
    String id=(String) model.getValueAt(i, 0);
    String nip=this.txt_nip.getText();
    String nama=this.txt_nama.getText();
    String jabatan = cJabatan.getSelectedItem().toString().trim();
            int pil1=0;
            String sJabatan="";
            pil1 = cJabatan.getSelectedIndex();
        if (pil1==0) { sJabatan="=== PILIH ===";}
        if (pil1==1) { sJabatan="Admin (Infoice)";}
        if (pil1==2) { sJabatan="Admin (Sales Support Keuangan)";}
        if (pil1==3) { sJabatan="Admin Service";}
        if (pil1==4) { sJabatan="Bagian Umum";}
        if (pil1==5) { sJabatan="Manajer";}
        if (pil1==6) { sJabatan="Magang";}
        if (pil1==7) { sJabatan="Salesman";}
        if (pil1==8) { sJabatan="SPM";}
        if (pil1==9) { sJabatan="SPG";}
        if (pil1==10) { sJabatan="Supervisor (Modern Market)";}
        if (pil1==11) { sJabatan="Tekhnisi";}
        
        String hadir=this.txt_hadir.getText();
        String sakit=this.txt_sakit.getText();
        String izin=this.txt_izin.getText();
        String tanpa_ket=this.txt_tanpa_ket.getText();
        String bulan = this.comboBulan.getSelectedItem().toString();
        int tahun = this.YearChooser.getYear();
        int total_lembur = Integer.parseInt(txttotallembur.getText().toString());
        
         try {
            Connection c=koneksi.getKoneksi();
            String sql = "UPDATE data_kehadiran SET nip=?,nama=?,nama_jab=?,hadir=?,sakit=?,izin=?,tanpa_ket=?,bulan=?,tahun=?,total_lembur=? WHERE id_absen=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, nip);
            p.setString(2, nama);
            p.setString(3, jabatan);
            p.setString(4, hadir);
            p.setString(5, sakit);  
            p.setString(6, izin);
            p.setString(7, tanpa_ket);
            p.setString(8, bulan);
            p.setInt(9, tahun);
            p.setInt(10, total_lembur);
            p.setString(11, id);
            p.executeUpdate();
            p.close();
            //HapusText();
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Opps Terjadi Kesalahan !!!");
        }finally{
            loadData();
        }
    }
    
    public void DeleteData() {
    int i=tblKehadiran.getSelectedRow();
        if(i==-1)
        {
            return;
        }
        String Id=(String) model.getValueAt(i, 0);
       
       
        try {
            Connection c=koneksi.getKoneksi();
            String sql = "DELETE FROM data_kehadiran WHERE id_absen=?";
            com.mysql.jdbc.PreparedStatement p=(com.mysql.jdbc.PreparedStatement) c.prepareStatement(sql);
            p.setString(1, Id);            
            p.executeUpdate();
            p.close();
        }catch(SQLException e){
            System.out.println("Terjadi kesalahan");
        }finally{
            loadData();
        
        }
    }
    private void reset_text(){
        txt_id_absen.setText("");
        txt_nip.setText("");
        txt_nama.setText("");
        cJabatan.setSelectedItem("=== PILIH ===");
        txt_hadir.setText("0");
        txt_izin.setText("0");
        txt_sakit.setText("0");
        txt_tanpa_ket.setText("0");
        comboBulan.setSelectedItem("");
        //YearChooser.setYear();
        txttotallembur.setText("");
    
    //progress
        setLocationRelativeTo(null);
        lblwait.setVisible(false);
        ProgressBar.setVisible(false);
    }
//buat tampil progress bar
   class TampilWorker extends SwingWorker{

        @Override
        protected Object doInBackground() throws Exception {
            for (int i = 0; i <= 100; i++) {
                try {
                    Thread.sleep(30);
                    ProgressBar.setVisible(true);
                    lblwait.setVisible(true);
                    ProgressBar.setValue(i);
                    ProgressBar.setIndeterminate(true);
                } catch (Exception e) {
                }
            }
            return null;
        } 
        @Override
        protected void done(){
            String id = txt_id_absen.getText();
            String nip = txt_nip.getText();
            String nama = txt_nama.getText();
            String jabatan = (String) cJabatan.getSelectedItem();
            String hadir = txt_hadir.getText();
            String izin = txt_izin.getText();
            String sakit = txt_sakit.getText();
            String tanpa_ket = txt_tanpa_ket.getText();
            ProgressBar.setVisible(false);
            lblwait.setVisible(false);
            //JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
            reset_text();
        }
   }
   public void mouseClicked(MouseEvent e) {
      ShowData();
   }
    public void ShowData() {
   
    int i=tblKehadiran.getSelectedRow();
        
        if(i==-1)
        {
            return;
        }
        String id=(String) model.getValueAt(i, 0);
        txt_id_absen.setText(id);
        String nip=(String) model.getValueAt(i, 1);
        txt_nip.setText(nip);
        String nama=(String) model.getValueAt(i, 2);
        txt_nama.setText(nama);
        String jabatan = (String) model.getValueAt(i, 3);
        cJabatan.setSelectedItem(jabatan);
        String hadir=(String) model.getValueAt(i, 4);
        txt_hadir.setText(hadir);
        String sakit=(String) model.getValueAt(i, 5);
        txt_sakit.setText(sakit);
        String izin=(String) model.getValueAt(i, 6);
        txt_izin.setText(izin);
        String tanpa_ket=(String) model.getValueAt(i, 7);
        txt_tanpa_ket.setText(tanpa_ket);
        String bulan=(String) model.getValueAt(i, 8);
        comboBulan.setSelectedItem(bulan);
       // String tahun=(String) model.getValueAt(i, 9);
        //YearChooser.setYear(tahun);
        String total_lembur=(String) model.getValueAt(i, 10);
        txttotallembur.setText(total_lembur);
}
    void filternama(KeyEvent a){
        if(Character.isDigit(a.getKeyChar())){
            a.consume();
            JOptionPane.showMessageDialog(rootPane,"Pada Kolom <'NAMA'> Hanya Bisa Memasukan Karakter Huruf", "< ERROR >", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_hadir = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_sakit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_izin = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_tanpa_ket = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        cJabatan = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblKehadiran = new javax.swing.JTable();
        btnReset = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblwait = new javax.swing.JLabel();
        ProgressBar = new javax.swing.JProgressBar();
        btnKembali = new javax.swing.JButton();
        txt_id_absen = new javax.swing.JTextField();
        txt_nip = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        comboBulan = new javax.swing.JComboBox();
        YearChooser = new com.toedter.calendar.JYearChooser();
        txttotallembur = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Edit Kehadiran");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nip");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nama");

        txt_nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_namaKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Jabatan");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Hadir");

        txt_hadir.setText("0");
        txt_hadir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_hadirKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_hadirKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Sakit");

        txt_sakit.setText("0");
        txt_sakit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_sakitKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_sakitKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Izin");

        txt_izin.setText("0");
        txt_izin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_izinKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_izinKeyTyped(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Tanpa Keterangan");

        txt_tanpa_ket.setText("0");
        txt_tanpa_ket.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_tanpa_ketKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_tanpa_ketKeyTyped(evt);
            }
        });

        btnUpdate.setText("Ubah");
        btnUpdate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setText("Hapus");
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        cJabatan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=== PILIH ===", "Admin (Infoice)", "Admin (Sales Support Keuangan)", "Admin Service", "Bagian Umum", "Manajer", "Magang", "Bagian Umum", "Salesman", "SPM", "SPG", "Supervisor (Modern Market)", "Tekhnisi" }));
        cJabatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cJabatanActionPerformed(evt);
            }
        });

        tblKehadiran.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblKehadiran.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblKehadiranMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblKehadiran);

        btnReset.setText("Bersih");
        btnReset.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("No");

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        lblwait.setText("Please wait....");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(233, Short.MAX_VALUE)
                .addComponent(lblwait)
                .addGap(211, 211, 211))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblwait)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnKembali.setText("Keluar");
        btnKembali.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Bulan");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Tahun");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Total Lembur");

        comboBulan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" }));
        comboBulan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Jam");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(43, 43, 43)
                        .addComponent(txt_hadir, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txt_sakit, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(txt_izin, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txt_tanpa_ket, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 656, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel8))
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnKembali, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(txt_nama, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(cJabatan, 0, 188, Short.MAX_VALUE))
                                            .addComponent(txt_nip, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(72, 72, 72))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(txt_id_absen, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(182, 182, 182)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txttotallembur, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel12))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel10))
                                        .addGap(47, 47, 47)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(YearChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(comboBulan, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(38, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_id_absen, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txt_nip, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cJabatan, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(comboBulan, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(YearChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txttotallembur, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_hadir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(txt_sakit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txt_izin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txt_tanpa_ket, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnKembali, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
       if(txt_id_absen.getText().equals("")||txt_nip.getText().equals("")||txt_nama.getText().equals("")||cJabatan.getSelectedItem().equals("")||txt_hadir.getText().equals("")||txt_izin.getText().equals("")||txt_sakit.getText().equals("")||txt_tanpa_ket.getText().equals("")||comboBulan.getSelectedItem().equals("")||txttotallembur.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih Data yang mau di update","Kesalahan", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
            TampilWorker tampilWorker = new TampilWorker();//show progress bar
        UpdateData();
        reset_text();
            tampilWorker.execute();
        try{
            JOptionPane.showMessageDialog(null, "Data Kehadiran Berhasil Di Update");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Data Gagal Di Update");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        if(txt_id_absen.getText().equals("")||txt_nip.getText().equals("")||txt_nama.getText().equals("")||cJabatan.getSelectedItem().equals("")||txt_hadir.getText().equals("")||txt_izin.getText().equals("")||txt_sakit.getText().equals("")||txt_tanpa_ket.getText().equals("")||comboBulan.getSelectedItem().equals("")||txttotallembur.getText().equals(""))
        {
        JOptionPane.showMessageDialog(null, "Pilih Data yang mau di hapus","Kesalahan", JOptionPane.ERROR_MESSAGE);
             return;
        } else
       {
    }
      TampilWorker tampilWorker = new TampilWorker();//show progress bar
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Hapus Data Ini ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){
           tampilWorker.execute();
            String Id=(String) this.txt_id_absen.getText();
            if ("".equals(Id.trim()) || Id.trim()==null)
            {
                return;
            }else{
                DeleteData();
                try{
                    JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Data Gagal Dihapus");
                }}} 
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void txt_hadirKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_hadirKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Hadir'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_hadirKeyTyped

    private void txt_sakitKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_sakitKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Sakit'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_sakitKeyTyped

    private void txt_izinKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_izinKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Izin'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_izinKeyTyped

    private void txt_tanpa_ketKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_tanpa_ketKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)){
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Type <'Tanpa Keterangan'> Harus Angka !!!", "Message", JOptionPane.WARNING_MESSAGE);
        }
        else{
        }
    }//GEN-LAST:event_txt_tanpa_ketKeyTyped

    private void tblKehadiranMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblKehadiranMouseClicked
        // TODO add your handling code here:
        ShowData();  
        this.txt_id_absen.enable(false);
        this.txt_nip.enable(false);
        
    }//GEN-LAST:event_tblKehadiranMouseClicked

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        // TODO add your handling code here:
        reset_text();
    }//GEN-LAST:event_btnResetActionPerformed

    private void cJabatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cJabatanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cJabatanActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        // TODO add your handling code here:
        int reply = JOptionPane.showConfirmDialog(
            null,
            "Keluar Data ?",
            "Message",
            JOptionPane.OK_CANCEL_OPTION);
        if(reply == JOptionPane.YES_OPTION){
        this.dispose();
        }
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void txt_namaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_namaKeyTyped
        // TODO add your handling code here:
        filternama(evt);
    }//GEN-LAST:event_txt_namaKeyTyped

    private void txt_hadirKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_hadirKeyReleased
        // TODO add your handling code here:
         String input=txt_hadir.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Hadir'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_hadir.setText("");
        }
        //
        validasino(evt);
    }//GEN-LAST:event_txt_hadirKeyReleased

    private void txt_sakitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_sakitKeyReleased
        // TODO add your handling code here:
         String input=txt_sakit.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Sakit'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_sakit.setText("");
        }
        //
        validasino(evt);
    }//GEN-LAST:event_txt_sakitKeyReleased

    private void txt_izinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_izinKeyReleased
        // TODO add your handling code here:
         String input=txt_izin.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Izin'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_izin.setText("");
        }
        //
        validasino(evt);
    }//GEN-LAST:event_txt_izinKeyReleased

    private void txt_tanpa_ketKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_tanpa_ketKeyReleased
        // TODO add your handling code here:
         String input=txt_tanpa_ket.getText();
        if(input.length()>2){
            JOptionPane.showMessageDialog(rootPane,"Jumlah input di kolom <'Tanpa Keterangan'> max hanya 2 Angka", "Warning", JOptionPane.WARNING_MESSAGE);
            txt_tanpa_ket.setText("");
        }
        //
        validasino(evt);
    }//GEN-LAST:event_txt_tanpa_ketKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UbahDataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UbahDataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UbahDataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UbahDataKehadiran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UbahDataKehadiran().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar ProgressBar;
    private com.toedter.calendar.JYearChooser YearChooser;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox cJabatan;
    private javax.swing.JComboBox comboBulan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblwait;
    private javax.swing.JTable tblKehadiran;
    private javax.swing.JTextField txt_hadir;
    private javax.swing.JTextField txt_id_absen;
    private javax.swing.JTextField txt_izin;
    private javax.swing.JTextField txt_nama;
    private javax.swing.JTextField txt_nip;
    private javax.swing.JTextField txt_sakit;
    private javax.swing.JTextField txt_tanpa_ket;
    private javax.swing.JTextField txttotallembur;
    // End of variables declaration//GEN-END:variables
  
    public void validasino(KeyEvent a){
         int hdr=0, skt=0, izn=0, tanpaket=0;
         if(txt_hadir.getText().equals("")){
             hdr=0;
         }else{
             hdr=Integer.parseInt(txt_hadir.getText());
         }
         
         if(txt_sakit.getText().equals("")){
             skt=0;
         }else{
             skt=Integer.parseInt(txt_sakit.getText());
         }
         
         if(txt_izin.getText().equals("")){
             izn=0;
         }else{
            izn=Integer.parseInt(txt_izin.getText());
         }
         
         if(txt_tanpa_ket.getText().equals("")){
             tanpaket=0;
         }else{
             tanpaket=Integer.parseInt(txt_tanpa_ket.getText());
         }
         int jml=hdr+skt+izn+tanpaket;
         System.out.println(jml);
         if (jml>25){
             a.consume();
             txt_hadir.setText("0");
             txt_sakit.setText("0");
             txt_izin.setText("0");
             txt_tanpa_ket.setText("0");
             JOptionPane.showMessageDialog(rootPane,"Hanya Bisa Menginputkan Kehadiran Sebesar 25 Hari", "Warning", JOptionPane.WARNING_MESSAGE);
         }
     }
}
