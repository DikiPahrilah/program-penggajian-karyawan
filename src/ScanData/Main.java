package ScanData;

import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FachrealHeart
 */
public class Main extends javax.swing.JFrame {
    private DefaultTableModel model;
    
    private LoadWorker worker;
    private Connection conn;

    public Main() throws ClassNotFoundException, SQLException {
        initComponents();
        
        model = new DefaultTableModel (){
        public boolean isCellEditable(int rowIndex, int colIndex) {//no edit the table
        return false;
        }
        };
        tblData.setModel(model);
        model.addColumn("No.");
        model.addColumn("Nip");
        model.addColumn("Nama");
        model.addColumn("Tempat Lahir");  
        model.addColumn("Tanggal Lahir"); 
        model.addColumn("Alamat");
        model.addColumn("Golongan");
        model.addColumn("Jab. Struktur");
        model.addColumn("Jab. Fungsional");
        model.addColumn("Pend. Akhir");
        model.addColumn("Status Kawin");
        model.addColumn("Jumlah Anak");
        /*
        Image icon = new javax.swing.ImageIcon(getClass().getResource("com/mkdika/swingworker/resources/icon/db.png")).getImage();
        setIconImage(icon);
        setLocationRelativeTo(null);
        */
        worker = new LoadWorker();
        worker.execute();
    }

    private class LoadWorker extends SwingWorker<Boolean, Void> {

        @Override
        protected void done() {
            try {
                if (get() != null) {
                    lblInfo.setText("Total data: " + tblData.getRowCount());
                    pgbMain.setIndeterminate(false);
                    btnRefresh.setEnabled(true);
                }
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        protected Boolean doInBackground() throws SQLException, InterruptedException {
            pgbMain.setIndeterminate(true);
            lblInfo.setText("Fetching data. . .");
            btnRefresh.setEnabled(false);

            if (conn == null) {
                conn = DriverManager.getConnection(DbConn.JDBC_URL,
                        DbConn.JDBC_USERNAME,
                        DbConn.JDBC_PASSWORD);
            }

            removeAllRow();
            String sql = "SELECT * FROM data_karyawan";
            PreparedStatement pstatement = conn.prepareStatement(sql);
            ResultSet rs = pstatement.executeQuery();
            if (rs.isBeforeFirst()) { // check is resultset not empty
                DefaultTableModel tableModel = (DefaultTableModel) tblData.getModel();
                while (rs.next()) {
                    Object data[] = {
                        rs.getInt("nomor"),
                        rs.getString("nip"),
                        rs.getString("nama"),
                        rs.getString("tempat_lahir"),
                        rs.getDate("tgl_lahir"),
                        rs.getString("alamat"),
                        rs.getString("golongan"),
                        rs.getString("jab_struktur"),
                        rs.getString("jab_fungsional"),
                        rs.getString("pend_akhir"),
                        rs.getString("status_kawin"),
                        rs.getString("jumlah_anak")
                    };
                    tableModel.addRow(data);
                }
            } else {
                lblInfo.setText("Data not found!");
            }
            rs.close();
            pstatement.close();
            Thread.sleep(3000);
            return true;
        }
    }

    private void removeAllRow() {
        DefaultTableModel tableModel = (DefaultTableModel) tblData.getModel();
        tableModel.setRowCount(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        pgbMain = new javax.swing.JProgressBar();
        lblInfo = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Data Karyawan");

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5"
            }
        ));
        jScrollPane1.setViewportView(tblData);

        pgbMain.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        lblInfo.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        lblInfo.setText("...");

        btnRefresh.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRefresh.setText("Refresh");
        btnRefresh.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnRefresh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pgbMain, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pgbMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblInfo, pgbMain});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        worker = new LoadWorker();
        worker.execute();
    }//GEN-LAST:event_btnRefreshActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws ClassNotFoundException {
        Class.forName(DbConn.JDBC_CLASS);

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Main().setVisible(true);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JProgressBar pgbMain;
    private javax.swing.JTable tblData;
    // End of variables declaration//GEN-END:variables
}
